package com.crystal.framework.resources;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;


public class Music implements Resource, OnCompletionListener, OnSeekCompleteListener, OnPreparedListener, OnVideoSizeChangedListener {
	MediaPlayer mediaPlayer;
	boolean isPrepared = false;
	boolean wasPlaying = false;
	float volume = 1;
	Audio audio;
	
	public Music(Audio audio, AssetFileDescriptor assetDescriptor) {
		this.audio = audio;
		mediaPlayer = new MediaPlayer();
		try {
			mediaPlayer.setDataSource(assetDescriptor.getFileDescriptor(), assetDescriptor.getStartOffset(), assetDescriptor.getLength());
			mediaPlayer.prepare();
			isPrepared = true;
			
			mediaPlayer.setOnCompletionListener(this);
			mediaPlayer.setOnSeekCompleteListener(this);
			mediaPlayer.setOnPreparedListener(this);
			mediaPlayer.setOnVideoSizeChangedListener(this);
		} catch (Exception e) {
			throw new RuntimeException("Couldn't load music:)");
		}
	}
	
	@Override
	public void dispose() {
		if (this.mediaPlayer.isPlaying()) {
			this.mediaPlayer.stop();
		}
		
		this.mediaPlayer.release();
	}
	
	public boolean isLooping() {
		return mediaPlayer.isLooping();
	}
	
	public boolean isPlaying() {
		return isPrepared && this.mediaPlayer.isPlaying();
	}
	
	public boolean wasPlaying() {
		return wasPlaying;
	}
	
	public boolean isStopped() {
		return !isPrepared;
	}
	
	public void pause() {
		if (this.mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
			wasPlaying = true;
		}
	}
	
	public void play() {
		if (this.mediaPlayer.isPlaying())
			return;
		
		try {
			synchronized (this) {
				if (!isPrepared) {
					mediaPlayer.prepare();
				}
				
				if (!wasPlaying)
					mediaPlayer.seekTo(0);
				
				mediaPlayer.setVolume(volume * audio.masterVolume, volume * audio.masterVolume);
				mediaPlayer.start();
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		wasPlaying = false;
	}
	
	public void setLooping(boolean isLooping) {
		mediaPlayer.setLooping(isLooping);
	}
	
	public void setVolume(float volume) {
		mediaPlayer.setVolume(volume, volume);
		this.volume = volume;
	}
	
	public void updateVolume(float volume) {
		mediaPlayer.setVolume(volume, volume);
	}
	
	public void stop() {
		if (this.mediaPlayer.isPlaying()) {
			
			synchronized (this) {
				this.mediaPlayer.stop();
				isPrepared = false;
			}
		}
	}
	
	@Override
	public void onCompletion(MediaPlayer player) {
		synchronized (this) {
			this.mediaPlayer.stop();
			isPrepared = false;
		}
	}
	
	public void seekBegin() {
		mediaPlayer.seekTo(0);
	}
	
	@Override
	public void onPrepared(MediaPlayer player) {
		// TODO Auto-generated method stub
		
		synchronized (this) {
			isPrepared = true;
		}
	}
	
	@Override
	public void onSeekComplete(MediaPlayer player) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void onVideoSizeChanged(MediaPlayer player, int width, int height) {
		// TODO Auto-generated method stub
	}
	
	public float getVolume() {
		return volume;
	}
}
