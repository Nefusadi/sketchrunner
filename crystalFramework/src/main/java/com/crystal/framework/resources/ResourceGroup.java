package com.crystal.framework.resources;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

import com.crystal.framework.Game;

class ResourceGroup {
	Dictionary<String, Resource> loadedResources = new Hashtable<String, Resource>();
	Game game;
	
	public ResourceGroup(Game game) {
		this.game = game;
	}
	
	public void registerResource(Resource r, String name) {
		Enumeration<String> keys = loadedResources.keys();
		boolean exists = false;
		while (keys.hasMoreElements()) {
			if (name == keys.nextElement()) {
				exists = true;
				break;
			}
		}
		
		if (exists) // prevent memory leaks
			loadedResources.get(name).dispose();
		
		loadedResources.put(name, r);
	}
	
	public void unloadResources() {
		
		Enumeration<String> keys = loadedResources.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			loadedResources.get(key).dispose();
			loadedResources.remove(key);
		}
	}
	
	public Resource get(String key) {
		return loadedResources.get(key);
	}
	
	public void changeVolume() {
		Enumeration<String> keys = loadedResources.keys();
		
		while (keys.hasMoreElements()) {
			Resource r = loadedResources.get(keys.nextElement());
			
			if (r.getClass().getName() == Music.class.getName()) {
				((Music)r).updateVolume(((Music)r).getVolume() * game.getAudio().masterVolume);
			} else if (r.getClass().getName() == Sound.class.getName()) {
				((Sound)r).updateVolume(((Sound)r).getVolume() * game.getAudio().masterVolumeSounds);
			}
		}
	}
	
	public void stopAllSounds() {
		Enumeration<String> keys = loadedResources.keys();
		
		while (keys.hasMoreElements()) {
			Resource r = loadedResources.get(keys.nextElement());
			
			if (r.getClass().getName() == Sound.class.getName())
				((Sound)r).stopAll();
		}
	}
	
	public void pause() {
		Enumeration<String> keys = loadedResources.keys();
		
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			
			Resource r = loadedResources.get(key);
			if (r.getClass().getName() == Music.class.getName()) {
				((Music)r).pause();
			}
		}
	}
	
	public void resume() {
		Enumeration<String> keys = loadedResources.keys();
		
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			
			Resource r = loadedResources.get(key);
			if (r.getClass().getName() == Music.class.getName() && ((Music)r).wasPlaying()) {
				((Music)r).play();
			}
		}
	}
}