package com.crystal.framework.resources;

import java.util.*;

import android.media.SoundPool;

public class Sound implements Resource {
	int soundId;
	SoundPool soundPool;
	float volume;
	Audio audio;
	boolean doLoop = false;
	
	List<Integer> streamIDs = new ArrayList<Integer>();
	
	public Sound(Audio audio, SoundPool soundPool, int soundId) {
		this.audio = audio;
		this.soundId = soundId;
		this.soundPool = soundPool;
	}
	
	public void play(float volume) {
		this.volume = volume;
		streamIDs.add(soundPool.play(soundId, volume * audio.masterVolumeSounds, volume * audio.masterVolumeSounds, 0, doLoop ? -1 : 0, 1));
	}
	
	public void stopAll() {
		for (int id : streamIDs) {
			soundPool.stop(id);
		}
		
		streamIDs.clear();
	}
	
	public void setLooping(boolean loop) {
		doLoop = loop;
		
		for (int id : streamIDs) {
			soundPool.setLoop(id, loop ? -1 : 0);
		}
	}
	
	public void updateVolume(float volume) {
		for (int id : streamIDs) {
			soundPool.setVolume(id, volume, volume);
		}
	}
	
	public float getVolume() {
		return volume;
	}
	
	@Override
	public void dispose() {
		soundPool.unload(soundId);
	}
}
