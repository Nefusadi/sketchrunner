package com.crystal.framework.resources;

import android.graphics.Bitmap;
import android.graphics.Rect;

import com.crystal.framework.Graphics.ImageFormat;

public class Sprite extends Image {
	
	int xFrames;
	int yFrames;
	float duration;
	
	int maxFrames;
	float width;
	float height;
	int roundWidth;
	int roundHeight;
	
	Rect frame = new Rect();
	int[] lefts;
	int[] tops;
	int[] rights;
	int[] bottoms;

	public Sprite(Bitmap bitmap, ImageFormat format, float scale, int sampleScale, int xFrames, int yFrames) {
		super(bitmap, format, scale, sampleScale);

		this.xFrames = xFrames;
		this.yFrames = yFrames;
		
		maxFrames = xFrames * yFrames;
		width = bitmap.getWidth() / (float)xFrames;
		height = bitmap.getHeight() / (float)yFrames;
		roundWidth = Math.round(width);
		roundHeight = Math.round(height);
		
		lefts   = new int[maxFrames];
		tops    = new int[maxFrames];
		rights  = new int[maxFrames];
		bottoms = new int[maxFrames];
		
		for (int i = 0; i < maxFrames; i++) {
			lefts[i]   = Math.round((i % xFrames) * width);
			tops[i]    = Math.round((i / xFrames) * height);
			rights[i]  = lefts[i] + roundWidth;
			bottoms[i] = tops[i] + roundHeight;
		}
	}
	
	public Rect getFrame(int frameNum) {
		
		if (frameNum >= maxFrames)
			frameNum = frameNum % maxFrames;
		else if (frameNum < 0)
			frameNum = 0;
		
		frame.left   = lefts[frameNum];
		frame.top    = tops[frameNum];
		frame.right  = rights[frameNum];
		frame.bottom = bottoms[frameNum];
		
		return frame;
	}
	
	public int getTotalFrames() {
		return maxFrames;
	}
	
	public int getXFrames() {
		return xFrames;
	}
	
	public int getYFrames() {
		return yFrames;
	}
	
	@Override
	public int getWidth() {
		return (int)(roundWidth * sampleScale / scale);
	}
	
	@Override
	public int getHeight() { return (int)(roundHeight * sampleScale / scale); }
	
	public int getImageWidth() {
		return super.getWidth();
	}
	
	public int getImageHeight() {
		return super.getHeight();
	}
}
