package com.crystal.framework.resources;

import java.io.IOException;


import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.AudioManager.OnAudioFocusChangeListener;

public class Audio {
	AssetManager assets;
	SoundPool soundPool;
	AudioManager am;
	
	AudioFocus audioFocus = AudioFocus.GAINED;
	
	float masterVolume = 1;
	float oldMasterVolume = 1;
	float masterVolumeSounds = 1;
	float oldMasterVolumeSounds = 1;
	
	boolean synchronizeMasterVolume = false;
	
	public static enum AudioFocus {
		LOST,
		DUCK,
		GAINED
	}
	
	public Audio(Activity activity) {
		activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		this.assets = activity.getAssets();
		this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
		am = (AudioManager)activity.getSystemService(Context.AUDIO_SERVICE);
	}
	
	public void setMasterVolume(float volume) {
		
		float soundVolume = synchronizeMasterVolume ? volume : this.masterVolumeSounds;

		setMasterVolume(volume, soundVolume);
		if (audioFocus == AudioFocus.LOST && volume != 0)
			requestAudioFocus();
	}
	
	public void setMasterVolumeSounds(float volume) {
		
		float musicVolume = synchronizeMasterVolume ? volume : this.masterVolume;
		
		setMasterVolume(musicVolume, volume);
		if (audioFocus == AudioFocus.LOST && synchronizeMasterVolume && volume != 0)
			requestAudioFocus();
	}
	
	private void setMasterVolume(float volumeMusic, float volumeSound) {
		
		if (audioFocus == AudioFocus.DUCK) {
			oldMasterVolume = volumeMusic;
			oldMasterVolumeSounds = volumeSound;
			
			masterVolume = volumeMusic * .5f;
			masterVolumeSounds = volumeSound * .5f;
		} else if (audioFocus == AudioFocus.LOST) {
			oldMasterVolume = volumeMusic;
			oldMasterVolumeSounds = volumeSound;
			masterVolumeSounds = volumeSound;
		}
		else {
			masterVolumeSounds = volumeSound;
			masterVolume = volumeMusic;
			
			oldMasterVolumeSounds = volumeSound;
			oldMasterVolume = volumeMusic;
		}
		
		ResourceManager.updateVolume();
	}
	
	public void requestAudioFocus() {
		int result = am.requestAudioFocus(audioListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

		if (result == AudioManager.AUDIOFOCUS_REQUEST_FAILED) {
	        if (audioFocus == AudioFocus.GAINED) {
	            oldMasterVolume = masterVolume;
	            oldMasterVolumeSounds = masterVolumeSounds;
	        }
			setMasterVolume(0);
			audioFocus = AudioFocus.LOST;
		} else {
    		audioFocus = AudioFocus.GAINED;
    		setMasterVolume(oldMasterVolume, oldMasterVolumeSounds);
		}
	}
	
	public void abandonAudioFocus() {
		am.abandonAudioFocus(audioListener);
		audioFocus = AudioFocus.LOST;
	}

	OnAudioFocusChangeListener audioListener = new OnAudioFocusChangeListener() {
		@Override
		public void onAudioFocusChange(int focusChange) {
	        if (audioFocus == AudioFocus.GAINED) {
	            oldMasterVolume = masterVolume;
	            oldMasterVolumeSounds = masterVolumeSounds;
	        }
	        
	        if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
	                // mute playback
	        		setMasterVolume(0, synchronizeMasterVolume ? 0 : masterVolumeSounds);
	        		audioFocus = AudioFocus.LOST;
	            }
	        	else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
	        		if (audioFocus == AudioFocus.GAINED) {
	        			setMasterVolume(masterVolume * .5f, masterVolumeSounds * .5f);
	        			audioFocus = AudioFocus.DUCK; // Only set the sounds if we're the app that needs to duck
	        		}
	        	}
	        	else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
	                // Resume playback 
	        		audioFocus = AudioFocus.GAINED;
	        		setMasterVolume(oldMasterVolume, oldMasterVolumeSounds);
	            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
	                setMasterVolume(0, synchronizeMasterVolume ? 0 : masterVolumeSounds);
	        		audioFocus = AudioFocus.LOST;
	            }

		}
	};
	
	public Music createMusic(String filename) {
		try {
			AssetFileDescriptor assetDescriptor = assets.openFd(filename);
			return new Music(this, assetDescriptor);
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load music '" + filename + "'");
		}
	}
	
	public Sound createSound(String filename) {
		try {
			AssetFileDescriptor assetDescriptor = assets.openFd(filename);
			int soundId = soundPool.load(assetDescriptor,  0);
			return new Sound(this, soundPool, soundId);
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load sound '" + filename + "'");
		}
	}
	
	public void setMusicSoundSync(boolean sync) {
		synchronizeMasterVolume = sync;
	}
	
	public float getMusicVolume() {
		return masterVolume;
	}
	
	public float getSoundVolume() {
		return masterVolumeSounds;
	}
	
	public AudioFocus getAudioFocus() {
		return audioFocus;
	}

	public void onDestroy() {
		soundPool.release();
	}
}
