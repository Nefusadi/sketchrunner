package com.crystal.framework.resources;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

import com.crystal.framework.Game;
import com.crystal.framework.Graphics.ImageFormat;

public class ResourceManager {
	
	static Dictionary<String, ResourceGroup> resourceGroups = new Hashtable<String, ResourceGroup>();
	static Game game;
	static boolean initialized = false;
	
	public static void Initialize(Game game) {
		ResourceManager.game = game;
		initialized = true;
	}
	
	static void updateVolume() {
		Enumeration<String> keys = resourceGroups.keys();
		
		while (keys.hasMoreElements()) {
			resourceGroups.get(keys.nextElement()).changeVolume();
		}
	}
	
	public static void stopAllSounds() {
		Enumeration<String> keys = resourceGroups.keys();
		
		while (keys.hasMoreElements()) {
			resourceGroups.get(keys.nextElement()).stopAllSounds();
		}
	}
	
	public static void pauseAllSounds() {
		game.getAudio().soundPool.autoPause();
	}
	
	public static void resumeAllSounds() {
		game.getAudio().soundPool.autoResume();
	}
	
	public static Resource get(String group, String name) {
		return resourceGroups.get(group).get(name);
	}
	
	public static void registerResource(String group, String name, Resource r) {
		synchronized(game) {
			boolean found = false;
			Enumeration<String> keys = resourceGroups.keys();
			while (keys.hasMoreElements()) {
				found = group == keys.nextElement();
				
				if (found)
					break;
			}
			
			if (!found)
				resourceGroups.put(group, new ResourceGroup(game));
			
			resourceGroups.get(group).registerResource(r, name);
		}
	}
	
	public static void unloadGroup(String group) {
		synchronized(game) {
			if (resourceGroups.get(group) == null)
				return;
			
			resourceGroups.get(group).unloadResources();
			resourceGroups.remove(group);
		}
	}
	
	public static void unloadAll() {
		synchronized(game) {
			Enumeration<String> keys = resourceGroups.keys();
			
			while (keys.hasMoreElements()) {
				String key = keys.nextElement();
				resourceGroups.get(key).unloadResources();
				resourceGroups.remove(key);
			}
		}
	}
	
	public static void loadImage(String fileName, ImageFormat format, int scale, String group, String name) {
		if (!initialized) return;
		
		Image image = game.getGraphics().newImage(fileName, format, scale);
		registerResource(group, name, image);
	}
	
	public static void loadSprite(String filename, ImageFormat format, int scale, String group, String name, int x, int y) {
		if (!initialized) return;
		
		Sprite sprite = game.getGraphics().newSprite(filename, format, scale, x, y);
		registerResource(group, name, sprite);
	}
	
	public static void loadSound(String filename, String group, String name) {
		if (!initialized) return;
		
		Sound sound = game.getAudio().createSound(filename);
		registerResource(group, name, sound);
	}
	
	public static void loadMusic(String filename, String group, String name) {
		if (!initialized) return;
		
		Music music = game.getAudio().createMusic(filename);
		registerResource(group, name, music);
	}
	
	public static void onPause() {
		Enumeration<String> groups = resourceGroups.keys();
		
		while (groups.hasMoreElements()) {
			resourceGroups.get(groups.nextElement()).pause();
		}

		pauseAllSounds();
	}
	
	public static void onResume() {
		Enumeration<String> groups = resourceGroups.keys();
		
		while (groups.hasMoreElements()) {
			resourceGroups.get(groups.nextElement()).resume();
		}
		
		resumeAllSounds();
	}
}
