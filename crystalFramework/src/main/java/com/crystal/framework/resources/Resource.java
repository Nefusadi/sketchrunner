package com.crystal.framework.resources;

public interface Resource {
	
	public void dispose();
}
