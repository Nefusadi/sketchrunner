package com.crystal.framework.resources;

import android.graphics.Bitmap;

import com.crystal.framework.Graphics.ImageFormat;

public class Image implements Resource {

	Bitmap bitmap;
	ImageFormat format;

	float scale;
	int sampleScale;

	public Image(Bitmap bitmap, ImageFormat format, float scale, int sampleScale) {
		this.bitmap = bitmap;
		this.format = format;
		this.scale = scale;
		this.sampleScale = sampleScale;
	}
	
	public int getWidth() { return Math.round(bitmap.getWidth() * sampleScale / scale); }

	public int getHeight() { return Math.round(bitmap.getHeight() * sampleScale / scale); }

	public int getSampleScale() { return sampleScale; }

	public ImageFormat getFormat() {
		return format;
	}
	
	public Bitmap getBitmap() {
		return bitmap;
	}

	@Override
	public void dispose() {
		bitmap.recycle();
	}

}
