package com.crystal.framework;

public class Vector {
	float x;
	float y;
	
	public static Vector zero() {
		return new Vector(0, 0);
	}
	
	public Vector(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public void scale(float scalar) {
		x *= scalar;
		y *= scalar;
	}
	
	public Vector add(Vector other) {
		float newX = x + other.getX();
		float newY = y + other.getY();
		
		return new Vector(newX, newY);
	}
	
	public Vector subtract(Vector other) {
		float newX = x - other.getX();
		float newY = y - other.getY();
		
		return new Vector(newX, newY);
	}
	
	// dot
	public float dot(Vector other) {
		return x * other.x + y * other.y;
	}
	
	// length
	public float length() {
		return (float)Math.sqrt(x * x + y * y);
	}
	
	public float lengthSquared() {
		return x * x + y * y;
	}
	
	// angle
	public float angle() {
		if (y == 0) {
			return x >= 0 ? 0 : (float)Math.PI;
		} else if (x == 0) {
			return y > 0 ? (float)(Math.PI / 2) : (float)(Math.PI * 3 / 2);
		}
		
		double ang = Math.atan(y / x);
		if (x < 0)
			ang += Math.PI;
		
		if (ang < 0)
			ang += 2 * Math.PI;
		else if (ang > 2 * Math.PI)
			ang = ang % (2 * Math.PI);
		
		return (float)ang;
	}
	
	public Vector copy() {
		return new Vector(x, y);
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
}
