package com.crystal.framework;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class AndroidGameLoop implements Runnable {
	Game game;
	Thread renderThread = null;
	final SurfaceHolder holder;
	volatile boolean running = false;
	
	public AndroidGameLoop(Game game, SurfaceHolder holder) {
		this.game = game;
		this.holder = holder;
	}
	
	public void resume() {
		running = true;
		renderThread = new Thread(this);
		renderThread.start();
	}

	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {}

		long startTime = System.nanoTime();
		while(running) {
			Canvas canvas = null;
			try {
				synchronized (holder) {
					canvas = holder.lockCanvas();
					if (canvas != null) {

						float deltaTime = (System.nanoTime() - startTime) / 1000000000f;
						startTime = System.nanoTime();

						if (deltaTime > 3.15) {
							deltaTime = 3.15f;
						}

						game.graphics.canvas = canvas;
						game.getCurrentScreen().update(deltaTime);
						game.getCurrentScreen().paint(deltaTime);
						game.graphics.canvas = null;
					}
				}
			}
			finally {
				if (canvas != null) {
					holder.unlockCanvasAndPost(canvas);
					game.graphics.canvas = null;
				}
			}
		}
	}

	public void pause() {
		running = false;

		while (true) {
			try {
				renderThread.join();
				break;
			} catch (InterruptedException e) {
				// retry
			}
		}
	}
}
