package com.crystal.framework;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.crystal.framework.Graphics;
import com.crystal.framework.Input;
import com.crystal.framework.Screen;
import com.crystal.framework.resources.Audio;
import com.crystal.framework.resources.FileIO;
import com.crystal.framework.resources.ResourceManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;

public abstract class Game extends Activity implements ConnectionCallbacks, OnConnectionFailedListener {
	FrameLayout gameScreen;
	RelativeLayout guiLayer;
	RelativeLayout lastGuiLayer;
	AndroidGameLoop gameLoopHandler;
	SurfaceView renderView;
	Graphics graphics;
	Audio audio;
	Input input;
	FileIO fileIO;
	Screen screen;

	private float scale = 1;
	
	private GoogleApiClient apiClient;
	private boolean resolvingError;
	protected boolean wasConnectedToGoogle;
    private static final String DIALOG_ERROR = "dialog_error";
	private static final int REQUEST_RESOLVE_ERROR = 1001;
	
	private static final String STATE_RESOLVING_ERROR = "resolving_error";
	private static final String STATE_WAS_CONNECTED = "was_connected";

	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    outState.putBoolean(STATE_RESOLVING_ERROR, resolvingError);
	    outState.putBoolean(STATE_WAS_CONNECTED, wasConnectedToGoogle);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// API Client Setup
		apiClient = new GoogleApiClient.Builder(this)
	    	.addApi(Games.API)
	    	.addScope(Games.SCOPE_GAMES)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
	    	.build();
		
	    resolvingError = savedInstanceState != null
	            && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);
	    wasConnectedToGoogle = savedInstanceState != null
	    		&& savedInstanceState.getBoolean(STATE_WAS_CONNECTED, false);
		// API Client Setup
		
		getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN | LayoutParams.FLAG_KEEP_SCREEN_ON, LayoutParams.FLAG_FULLSCREEN | LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		int frameBufferWidth = isPortrait ? 800 : 1280;
		int frameBufferHeight = isPortrait ? 1280 : 800;
		
		scale = isPortrait ? (float)frameBufferWidth / metrics.widthPixels :
							 (float)frameBufferHeight / metrics.heightPixels;
		
		if (isPortrait)
			frameBufferHeight = (int)(metrics.heightPixels * scale);
		else
			frameBufferWidth = (int)(metrics.widthPixels * scale);

		ResourceManager.Initialize(this);

		gameScreen = new FrameLayout(this);
		renderView = new SurfaceView(this);
		gameScreen.addView(renderView, metrics.widthPixels, metrics.heightPixels);
		gameLoopHandler = new AndroidGameLoop(this, renderView.getHolder());
		graphics = new Graphics(getAssets(), frameBufferWidth, frameBufferHeight, 1/scale);
		fileIO = new FileIO(this);
		audio = new Audio(this);
		input = new Input(this, renderView, scale, scale);
		screen = getInitScreen();
		setContentView(gameScreen);
	}
	
	public void setGuiLayer(RelativeLayout layout) {
	
		lastGuiLayer = guiLayer;
	    guiLayer = layout;
	    
	    this.runOnUiThread(new Runnable(){
	    	public void run(){
	    		if (lastGuiLayer != null) {
	    			gameScreen.removeView(lastGuiLayer);
	    			lastGuiLayer = null;
	    		}
		
	    		if (guiLayer == null) {
	    			return;
	    		}
		
	    		gameScreen.addView(guiLayer, 1);

	    		guiLayer.bringToFront();

	    	}
		});
	}
	
	@Override
	public void onResume() {
		super.onResume();
		gameLoopHandler.resume();
		ResourceManager.onResume();
		screen.resume();
        getAudio().requestAudioFocus();
	}
	
	@Override
	public void onPause() {
		gameLoopHandler.pause();
		super.onPause();
		screen.pause();
		ResourceManager.onPause();
        getAudio().abandonAudioFocus();
		
		if (isFinishing())
			screen.dispose();
	}
	
    @Override
    protected void onStart() {
        super.onStart();
        
		if (wasConnectedToGoogle)
			attemptConnectToGooglePlayService();
    }
    
    public void attemptConnectToGooglePlayService() {
        if (!resolvingError) {
            apiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        apiClient.disconnect();
        super.onStop();
    }
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		audio.abandonAudioFocus();
		audio.onDestroy();
		ResourceManager.unloadAll();
	}
	
	public Input getInput() {
		return input;
	}
	
	public FileIO getFileIO() {
		return fileIO;
	}
	
	public Graphics getGraphics() {
		return graphics;
	}
	
	public Audio getAudio() {
		return audio;
	}
	
	public void setScreen(Screen screen) {
		if (screen == null)
			throw new IllegalArgumentException("Screen must not be null");
		
		this.screen.pause();
		this.screen.dispose();
		screen.resume();
		screen.update(0);
		this.screen = screen;
	}
	
	public Screen getCurrentScreen() {
		return screen;
	}
	
	public abstract Screen getInitScreen();
	
	public void onBackPressed()
	{
		screen.backButton();
	}
	
	public void quit() {
		finish();
	}
    
    public GoogleApiClient getApiClient() {
    	return apiClient;
    }
    
    //Google API Client Error Handling
    @Override
    public void onConnected(Bundle connectionHint) {
    	wasConnectedToGoogle = true;
    }
    
    @Override
    public void onConnectionSuspended(int cause) {
    	wasConnectedToGoogle = false;
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (resolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                resolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                apiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(result.getErrorCode());
            resolvingError = true;
        }
    }
    
    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
    	wasConnectedToGoogle = false;
    	
    	super.startActivityForResult(intent, requestCode);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            resolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!apiClient.isConnecting() &&
                        !apiClient.isConnected()) {
                    apiClient.connect();
                }
            }
        } else if (resultCode == GamesActivityResultCodes.RESULT_RECONNECT_REQUIRED)
        	wasConnectedToGoogle = false;
        else if (apiClient.isConnected())
        	wasConnectedToGoogle = true;
    }

    // The rest of this code is all about building the error dialog

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        resolvingError = false;
    }

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        private String DIALOG_ERROR;

		public ErrorDialogFragment() { }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((Game)getActivity()).onDialogDismissed();
        }
    }
    // Google API Client ErrorHandling
}
