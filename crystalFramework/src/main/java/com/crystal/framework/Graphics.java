package com.crystal.framework;

import java.io.IOException;
import java.io.InputStream;

import com.crystal.framework.resources.*;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;

public class Graphics {
	
	public enum ImageFormat {
		RGB565,
		ARGB4444,
		ARGB8888,
		A8
	}
	
	AssetManager assets;
	Canvas canvas;
	Paint paint;
	Rect srcRect = new Rect();
	Rect dstRect = new Rect();
	private float scale;

	private int width;
	private int height;
	
	public Graphics(AssetManager assets, int width, int height, float scale) {
		this.assets = assets;
		this.paint = new Paint();
		this.width = width;
		this.height = height;
		this.scale = scale;
	}
	
	public Image newImage(String fileName, ImageFormat format, int scale) {
		Config config = null;
		if (format == ImageFormat.RGB565)
			config = Config.RGB_565;
		else if (format == ImageFormat.ARGB4444)
			config = Config.ARGB_4444;
		else if (format == ImageFormat.A8)
			config = Config.ALPHA_8;
		else
			config = Config.ARGB_8888;
		
		Options options = new Options();
		options.inPreferredConfig = config;
		options.inSampleSize = scale;
		
		InputStream in = null;
		Bitmap bitmap = null;
		try {
			in = assets.open(fileName);
			bitmap = BitmapFactory.decodeStream(in, null, options);
			if (bitmap == null)
				throw new RuntimeException("Couldn't load bitmap from asset '" + fileName + "'");
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load bitmap from asset '" + fileName + "'");
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					
				}
			}
		}

		if (this.scale != 1) {
			Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap,
					(int) (bitmap.getWidth() * this.scale),
					(int) (bitmap.getHeight() * this.scale),
					true);
			bitmap.recycle();
			bitmap = scaledBitmap;
		}

		System.gc();
		
		if (bitmap.getConfig() == Config.RGB_565)
			format = ImageFormat.RGB565;
		else if (bitmap.getConfig() == Config.ARGB_4444)
			format = ImageFormat.ARGB4444;
		else if (bitmap.getConfig() == Config.ALPHA_8)
			format = ImageFormat.A8;
		else
			format = ImageFormat.ARGB8888;
		
		return new Image(bitmap, format, this.scale, scale);
	}
	
	public Sprite newSprite(String fileName, ImageFormat format, int scale, int x, int y) {
		Image i = newImage(fileName, format, scale);

		return new Sprite(i.getBitmap(), i.getFormat(), this.scale, scale, x, y);
	}
	
	public void clearScreen(int color) {
		canvas.drawRGB((color & 0xff0000) >> 16, (color & 0xff00) >> 8, (color & 0xff));
	}
	
	public void drawLine(int x, int y, int x2, int y2, int color, float thickness) {
		paint.setColor(color);
		paint.setStrokeWidth(thickness);
		canvas.drawLine((int)(x * scale),  (int)(y * scale),  (int)(x2 * scale),  (int)(y2 * scale),  paint);
	}
	
	public void drawRect(int x, int y, int width, int height, int color) {
		paint.setColor(color);
		paint.setStyle(Style.FILL);
		canvas.drawRect((int)(x * scale),  (int)(y * scale), (int)((x + width) * scale), (int)((y + height) * scale), paint);
	}
	
	public void drawARGB(int a, int r, int g, int b) {
		paint.setStyle(Style.FILL);
		canvas.drawARGB(a,  r,  g,  b);
	}
	
	public void drawString(String text, int x, int y, Paint paint) {
		float textSize = paint.getTextSize();
		paint.setTextSize(textSize * scale);
		canvas.drawText(text,  (int)(x * scale),  (int)(y * scale),  paint);
		paint.setTextSize(textSize);
	}
	
	public void drawImage(Image image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight) {
		paint.setColor(Color.WHITE);

		int sampleScale = image.getSampleScale();
		
		srcRect.left = srcX;
		srcRect.top = srcY;
		srcRect.right = srcX + Math.round(srcWidth * scale);
		srcRect.bottom = srcY + Math.round(srcHeight * scale);
		
		dstRect.left = (int)(x * scale);
		dstRect.top = (int)(y * scale);
		dstRect.right = (int)(x * scale + srcWidth * sampleScale * scale);
		dstRect.bottom = (int)(y * scale + srcHeight * sampleScale * scale);

		canvas.drawBitmap(image.getBitmap(), srcRect, dstRect, null);
	}
	
	public void drawImage(Image image, int x, int y) {
		paint.setColor(Color.WHITE);
		if (image.getSampleScale() == 1)
			canvas.drawBitmap(image.getBitmap(), (int)(x), (int)(y), paint);
		else
			drawImage(image, x, y, 0, 0, image.getWidth(), image.getHeight());
	}
	
	public void drawSprite(Sprite sprite, int frame, int x, int y) {
		paint.setColor(Color.WHITE);
		
		srcRect = sprite.getFrame(frame);
		
		dstRect.left = (int)(x * scale);
		dstRect.top = (int)(y * scale);
		dstRect.bottom = (int)(y * scale + sprite.getHeight() * scale);
		dstRect.right = (int)(x * scale + sprite.getWidth() * scale);
		
		canvas.drawBitmap(sprite.getBitmap(), srcRect, dstRect, paint);
	}
	
	public void drawScaledImage(Image image, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight) {
		paint.setColor(Color.WHITE);
		
		srcRect.left = srcX;
		srcRect.top = srcY;
		srcRect.right = srcX + srcWidth;
		srcRect.bottom = srcY + srcHeight;
		
		dstRect.left = (int)(x * this.scale);
		dstRect.top = (int)(y * this.scale);
		dstRect.right = (int)(x * scale + width * scale);
		dstRect.bottom = (int)(y * scale + height * scale);
		
		canvas.drawBitmap(image.getBitmap(), srcRect, dstRect, null);
	}
	
	public void drawScaledSprite(Sprite sprite, int frame, int x, int y, int width, int height) {
		paint.setColor(Color.WHITE);
		
		srcRect = sprite.getFrame(frame);
		
		dstRect.left = (int)(x * this.scale);
		dstRect.top = (int)(y * this.scale);
		dstRect.bottom = (int)(y * scale + height * scale);
		dstRect.right = (int)(x * scale + width * scale);
		
		canvas.drawBitmap(sprite.getBitmap(), srcRect, dstRect, paint);
	}
	
	public int getWidth() { return width; }
	
	public int getHeight() { return height; }
}
