package com.crystal.framework;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public abstract class Screen {
	
	protected final Game game;
	
	public Screen(Game game) {
		this.game = game;
	}
	
	public RelativeLayout getLayoutFromId(int id) {

		LayoutInflater layoutInflater = (LayoutInflater) 
		        game.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return (RelativeLayout)layoutInflater.inflate(id, (ViewGroup)game.findViewById(id), false);
	}
	
	public abstract void update(float deltaTime);
	
	public abstract void paint(float deltaTime);
	
	public abstract void pause();
	
	public abstract void resume();
	
	public abstract void dispose();
	
	public abstract void backButton();
}
