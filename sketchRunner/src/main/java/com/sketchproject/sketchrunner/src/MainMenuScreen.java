package com.sketchproject.sketchrunner.src;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.crystal.framework.Game;
import com.crystal.framework.Graphics;
import com.crystal.framework.Screen;
import com.crystal.framework.resources.*;
import com.google.android.gms.games.Games;

import com.sketchproject.sketchrunner.R;

public class MainMenuScreen extends Screen {

	RelativeLayout layout;
	
	public MainMenuScreen(Game game) {
		super(game);

		((Music)ResourceManager.get("Menu", "BackgroundMusic")).play();
		//((Music)ResourceManager.get("Menu", "BackgroundMusic")).seekBegin();
		
		// Create the layout for the gui
		layout = getLayoutFromId(R.layout.main_menu_gui);
		
		game.setGuiLayer(layout);
		
		// setup the events for the gui
		ImageButton b = (ImageButton)layout.findViewById(R.id.playButton);
		b.setOnClickListener(new OnClickListener () {

			@Override
			public void onClick(View v) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				goToGame();
			}
		});
		
		ImageButton quitButton = (ImageButton)layout.findViewById(R.id.quitButton);
		quitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				backButton();
			}
			
		});
		
		ImageButton musicButton = (ImageButton)layout.findViewById(R.id.imageButton2);
		musicButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				SeekBar musicBar = (SeekBar)layout.findViewById(R.id.MusicBar);
				SeekBar soundBar = (SeekBar)layout.findViewById(R.id.SoundBar);
				
				if (musicBar.getVisibility() == View.GONE) {
					musicBar.setVisibility(View.VISIBLE);
					soundBar.setVisibility(View.GONE);
				}
				else
					musicBar.setVisibility(View.GONE);
			}
		});
		
		SeekBar musicBar = (SeekBar)layout.findViewById(R.id.MusicBar);
		musicBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if (arg2)
					setMusic(arg1 / 100f);
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {

				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {

				
			}
			
		});
		musicBar.setProgress((int)(game.getAudio().getMusicVolume() * 100));
		
		ImageButton soundButton = (ImageButton)layout.findViewById(R.id.imageButton1);
		soundButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				SeekBar soundBar = (SeekBar)layout.findViewById(R.id.SoundBar);
				SeekBar musicBar = (SeekBar)layout.findViewById(R.id.MusicBar);
				
				if (soundBar.getVisibility() == View.GONE) {
					soundBar.setVisibility(View.VISIBLE);
					musicBar.setVisibility(View.GONE);
				}
				else
					soundBar.setVisibility(View.GONE);
			}
		});
		
		SeekBar soundBar = (SeekBar)layout.findViewById(R.id.SoundBar);
		soundBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if (arg2)
					setSound(arg1 / 100f);
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {

				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {

				
			}
			
		});
		soundBar.setProgress((int)(game.getAudio().getSoundVolume() * 100));
		
		ImageButton openScoreView = (ImageButton)layout.findViewById(R.id.showScoreButton);
		openScoreView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				int[] highScores = getHighScores();
				
				TextView scores = (TextView)layout.findViewById(R.id.ScoreText1);
				scores.setText((highScores[0] == -1 ? "" : String.valueOf(highScores[0])) + "\n"
						+ (highScores[1] == -1 ? "" : String.valueOf(highScores[1])) + "\n"
						+ (highScores[2] == -1 ? "" : String.valueOf(highScores[2])) + "\n"
						+ (highScores[3] == -1 ? "" : String.valueOf(highScores[3])) + "\n"
						+ (highScores[4] == -1 ? "" : String.valueOf(highScores[4])));
				
				scores = (TextView)layout.findViewById(R.id.ScoreText2);
				scores.setText((highScores[5] == -1 ? "" : String.valueOf(highScores[5])) + "\n"
						+ (highScores[6] == -1 ? "" : String.valueOf(highScores[6])) + "\n"
						+ (highScores[7] == -1 ? "" : String.valueOf(highScores[7])) + "\n"
						+ (highScores[8] == -1 ? "" : String.valueOf(highScores[8])) + "\n"
						+ (highScores[9] == -1 ? "" : String.valueOf(highScores[9])));
				
				RelativeLayout HighScoreView = (RelativeLayout)layout.findViewById(R.id.HighScoresWindow);
				HighScoreView.setVisibility(View.VISIBLE);
			}
			
		});
		
		ImageButton closeScoreView = (ImageButton)layout.findViewById(R.id.closeScoreWindowButton);
		closeScoreView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				RelativeLayout HighScoreView = (RelativeLayout)layout.findViewById(R.id.HighScoresWindow);
				HighScoreView.setVisibility(View.GONE);
			}
			
		});
		
		Button connectButton = (Button)layout.findViewById(R.id.viewLeaderboard);
		if (((RunnerGame)game).getConnectToGoogle() && !game.getApiClient().isConnected()) {
			game.attemptConnectToGooglePlayService();
		} else if (game.getApiClient().isConnected())
			updateGooglePlayGUI();
		
		connectButton.setOnClickListener(new OnClickListener() {
	
			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				displayLeaderboard();
			}
			
		});
		
		Button achievementButton = (Button)layout.findViewById(R.id.viewAchievements);
		
		achievementButton.setOnClickListener(new OnClickListener() {
	
			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				displayAchievements();
			}
			
		});
	}
	
	public void updateGooglePlayGUI() {

		game.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Button connectButton = (Button)layout.findViewById(R.id.viewLeaderboard);
				Button achievementButton = (Button)layout.findViewById(R.id.viewAchievements);
				
				if (game.getApiClient().isConnected()) {
					connectButton.setText("View Leaderboard");
					achievementButton.setText("Achievements");
				} else {
					connectButton.setText("Connect to Leaderboard");
					achievementButton.setText("Connect to Achievements");
				}
				
			}
			
		});
	}

	void displayLeaderboard() {
		if (game.getApiClient().isConnected())
			game.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(game.getApiClient(), game.getString(R.string.leaderboard_high_scores)), 0);
		else
			game.attemptConnectToGooglePlayService();
	}
	
	void displayAchievements() {
		if (game.getApiClient().isConnected())
			game.startActivityForResult(Games.Achievements.getAchievementsIntent(game.getApiClient()), 0);
		else
			game.attemptConnectToGooglePlayService();
	}
	
	int[] getHighScores() {
		return ((RunnerGame)game).getScores();
	}
	
	void setMusic(float music) {
		game.getAudio().setMasterVolume(music);
	}
	
	void setSound(float sound) {
		game.getAudio().setMasterVolumeSounds(sound);
	}
	
	void goToGame() {
		((Music)ResourceManager.get("Menu", "BackgroundMusic")).stop();
		game.setScreen(new GameScreen(game));
	}

	@Override
	public void update(float deltaTime) {

		int volume = (int)(game.getAudio().getMusicVolume() * 100 * (game.getAudio().getAudioFocus() == Audio.AudioFocus.DUCK ? 2 : 1));
		SeekBar musicBar = (SeekBar)layout.findViewById(R.id.MusicBar);
		if (volume != musicBar.getProgress() && game.getAudio().getAudioFocus() != Audio.AudioFocus.LOST) {
			game.runOnUiThread(new Runnable() {

				@Override
				public void run() {

					int volume = (int)(game.getAudio().getMusicVolume() * 100 * (game.getAudio().getAudioFocus() == Audio.AudioFocus.DUCK ? 2 : 1));
					SeekBar musicBar = (SeekBar)layout.findViewById(R.id.MusicBar);
					musicBar.setProgress(volume);
				}
			});
		}
		
		volume = (int)(game.getAudio().getSoundVolume() * 100 * (game.getAudio().getAudioFocus() == Audio.AudioFocus.DUCK ? 2 : 1));
		SeekBar soundBar = (SeekBar)layout.findViewById(R.id.SoundBar);
		if (volume != soundBar.getProgress() && game.getAudio().getAudioFocus() != Audio.AudioFocus.LOST) {
			game.runOnUiThread(new Runnable() {

				@Override
				public void run() {

					int volume = (int)(game.getAudio().getSoundVolume() * 100 * (game.getAudio().getAudioFocus() == Audio.AudioFocus.DUCK ? 2 : 1));
					SeekBar soundBar = (SeekBar)layout.findViewById(R.id.SoundBar);
					soundBar.setProgress(volume);
				}
			});
		}
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		
		Image bg = (Image)ResourceManager.get("Menu", "Background");
		g.drawImage(bg, g.getWidth() / 2 - bg.getWidth() / 2, 0);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		
	}

	@Override
	public void backButton() {
		RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.HighScoresWindow);
		if (window.getVisibility() == View.VISIBLE)
			game.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.HighScoresWindow);
					window.setVisibility(View.GONE);
				}
				
			});
		else
			game.quit();
	}

}
