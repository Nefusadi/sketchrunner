package com.sketchproject.sketchrunner.src.shapes;

import android.graphics.Rect;

import com.crystal.framework.Graphics;
import com.crystal.framework.resources.ResourceManager;
import com.crystal.framework.resources.Sound;
import com.crystal.framework.resources.Sprite;
import com.sketchproject.sketchrunner.src.Animation;
import com.sketchproject.sketchrunner.src.StickMan;

public class Collectable implements Collidable {
	
	public final static int width = 100;
	public final static int height = 147;
	
	int x, y;
	
	Animation anim;
	float bounceTime = 0;
	boolean collected = false;
	
	Rect bounds = new Rect();
	
	static float animStartOffset = 0;
	static final int totalFrames = ((Sprite)ResourceManager.get("Game", "Collectable")).getTotalFrames();
	
	public Collectable(int x, int y, long totalDistance) {
		this.x = x;
		this.y = y;
		
		long totalX = x + totalDistance;
		totalX = (int)((totalX / (double)width) * 5);
		totalX += Math.round(animStartOffset);
		totalX %= totalFrames * 4;
		
		bounceTime = (float)totalX / totalFrames;
		int speedDir = (int)bounceTime % 2 == 0 ? 1 : -1;
		
		anim = new Animation((Sprite)ResourceManager.get("Game", "Collectable"));
		anim.setSpeed(.5f * speedDir);
		
		int frame = (int)(totalX % totalFrames);
		if (anim.getSpeed() < 0)
			frame = totalFrames - frame - 1;
		
		anim.setFrame(frame);
	}
	
	@Override
	public Rect getBounds() {
		
		bounds.left = x;
		bounds.top = y;
		bounds.right = x + width;
		bounds.bottom = y + width;
		return bounds;
	}

	@Override
	public boolean collide(StickMan stickMan) {
		if (collected)
			return false;
		
		stickMan.incrementCollection();
		collected = true;
		((Sound)ResourceManager.get("Sounds", "collect")).play(1);

		return false;
	}

	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub
		x = x - (int)(StickMan.runSpeed * deltaTime);
		
		anim.update(deltaTime);
		
		bounceTime += deltaTime;
		if (bounceTime > 4)
			bounceTime -= 4;
		if (anim.getCurrentFrame() >= totalFrames && anim.getSpeed() > 0 || anim.getCurrentFrame() <= 0 && anim.getSpeed() < 0) {
			anim.setSpeed(anim.getSpeed() * -1);
			anim.setFrame(anim.getCurrentFrame() + (int)(1 * anim.getSpeed() / Math.abs(anim.getSpeed())));
		}
	}
	
	public static void updateAnimStart(float deltaTime) {
		animStartOffset += deltaTime * 15;

		animStartOffset %= totalFrames;
	}

	@Override
	public void paint(float deltaTime, Graphics g) {
		
		if (collected)
			return;
		
		anim.paint(g, x, y + (int)(Math.sin(bounceTime * Math.PI) * width / 4), width, width);
	}
	
	@Override
	public void paintPartial(float percent, Graphics g) {
		
	}

}
