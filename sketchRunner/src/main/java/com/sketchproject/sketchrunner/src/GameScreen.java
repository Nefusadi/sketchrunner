package com.sketchproject.sketchrunner.src;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.crystal.framework.*;
import com.crystal.framework.Input.TouchEvent;
import com.crystal.framework.resources.*;
import com.google.android.gms.games.Games;
import com.sketchproject.sketchrunner.R;
import com.sketchproject.sketchrunner.src.shapes.*;

public class GameScreen extends Screen {

	private final static int transparentBlack = Color.parseColor("#70000000");
	
	int inTut = -1;
	float tutTimer = 0;
	
	Animation sharkAnim;
	int sharkX;
	float sharkTime = 0;
	boolean scoreShown = false;
	boolean chompPlayed = false;
	
	StickMan stickMan;
	List<Line> currentDraw = new ArrayList<Line>();
	int firstTouchX = -1;
	int firstTouchY = -1;
	boolean hasFirstTouch = false;
	int currentPointer = -1;
	List<Collidable> env = new ArrayList<Collidable>();
	
	boolean initializing = true;
	boolean drawingEnvironment = true;
	boolean playedFootstepSound = false;
	boolean softPause = false;
	int envAnimIndex = 0;
	float drawTime = 0;
	final float timePerLine = .25f;
	
	int screenWidth = game.getGraphics().getWidth();
	int screenHeight = game.getGraphics().getHeight();
	
	int zoneHeight = StickMan.height + 50;
	int minWidth = 350;
	
	final int maxDrawSpace = 700;
	final int minDrawSpace = maxDrawSpace - zoneHeight * 5 / 2;
	
	int drawPosX = 1000;
	int drawPosY = maxDrawSpace;
	
	int lastGroundEnd;
	boolean drewPitLast = false;
	int lastCeilingEnd = 7500; // requires this much distance before ceilings start generating
	
	long totalDistanceTraveled = 0;
	
	int lastScore = -1;
	
	Random rand = new Random();
	
	int distancePerDifficulty = 10000;
	// Probabilities of shapes
	int gap = 0;
	int drop = 15;
	int ground = 5;
	int platform = 5;
	int jump = 15;
	int wall = 0;
	int box = 0;
	int ramp = 0;
	int walledRamp = 0;
	int rampPit = 0;
	int tunnel = 0; boolean upsideDownTunnel = false;
	int hole = 0;
	// Probabilities of shapes
	
	RelativeLayout layout;
	boolean wasInPause = false;
	
	public int getCurrentScore() {
		return (int)(totalDistanceTraveled / 155) + stickMan.collectionCount + stickMan.bonusPoints;
	}
	
	int totalProbabilityCount() {
		return
			  gap
			+ drop
			+ ground
			+ platform
			+ jump
			+ wall
			+ box
			+ ramp
			+ walledRamp
			+ rampPit
			+ tunnel
			+ hole;
	}
	
	float clamp (float f, float min, float max) {
		if (f < min)
			return min;
		
		if (f > max)
			return max;
		
		return f;
	}
	
	void updateProbabilities() {
		int Tier = (int)(totalDistanceTraveled / distancePerDifficulty);
		gap		   = gap		< 0 || Tier < 0 ? 0 : gap		 + 1;//clamp((int)(totalDistanceTraveled / 100), 0, 20);
		drop	   = drop		< 0 || Tier < 0 ? 0 : drop		 + 1;//clamp((int)(totalDistanceTraveled / 100), 0, 20);
		ground	   = ground		< 0 || Tier < 0 ? 0 : ground	 + 1;//clamp((int)(totalDistanceTraveled / 500), 0, 25);
		platform   = platform	< 0 || Tier < 0 ? 0 : platform	 + 1;//clamp((int)(totalDistanceTraveled / 500), 0, 15);
		jump	   = jump		< 0 || Tier < 0 ? 0 : jump		 + 1;//clamp((int)(totalDistanceTraveled / 100), 0, 20);
		wall	   = wall		< 0 || Tier < 4 ? 0 : wall		 + 7;//clamp((int)(totalDistanceTraveled / 5000), 0, 30);
		box		   = box		< 0 || Tier < 3 ? 0 : box		 + 6;//clamp((int)(totalDistanceTraveled / 1000), 0, 10);
		ramp	   = ramp		< 0 || Tier < 1 ? 0 : ramp		 + 2;//clamp((int)(totalDistanceTraveled / 750), 0, 15);
		walledRamp = walledRamp < 0 || Tier < 2 ? 0 : walledRamp + 3;//clamp((int)(totalDistanceTraveled / 750), 0, 7);
		rampPit	   = rampPit	< 0 || Tier < 3 ? 0 : rampPit	 + 4;//clamp((int)(totalDistanceTraveled / 1000), 0, 7);
		tunnel	   = tunnel		< 0 || Tier < 3 ? 0 : tunnel	 + 4;//clamp((int)(totalDistanceTraveled / 2500), 0, 7);
		hole	   = hole		< 0 || Tier < 6 ? 0 : hole		 + 5;
	}
	
	OnClickListener scoreButtonListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			
			((Sound)ResourceManager.get("Sounds", "click")).play(1);
			
			int[] highScores = getHighScores();
			
			TextView scores = (TextView)layout.findViewById(R.id.ScoreText1);
			scores.setText((highScores[0] == -1 ? "" : String.valueOf(highScores[0])) + "\n"
					+ (highScores[1] == -1 ? "" : String.valueOf(highScores[1])) + "\n"
					+ (highScores[2] == -1 ? "" : String.valueOf(highScores[2])) + "\n"
					+ (highScores[3] == -1 ? "" : String.valueOf(highScores[3])) + "\n"
					+ (highScores[4] == -1 ? "" : String.valueOf(highScores[4])));
			
			scores = (TextView)layout.findViewById(R.id.ScoreText2);
			scores.setText((highScores[5] == -1 ? "" : String.valueOf(highScores[5])) + "\n"
					+ (highScores[6] == -1 ? "" : String.valueOf(highScores[6])) + "\n"
					+ (highScores[7] == -1 ? "" : String.valueOf(highScores[7])) + "\n"
					+ (highScores[8] == -1 ? "" : String.valueOf(highScores[8])) + "\n"
					+ (highScores[9] == -1 ? "" : String.valueOf(highScores[9])));
			
			RelativeLayout HighScoreView = (RelativeLayout)layout.findViewById(R.id.HighScoresWindow);
			HighScoreView.setVisibility(View.VISIBLE);
			
			RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
			window.setVisibility(View.GONE);

			window = (RelativeLayout)layout.findViewById(R.id.ScoreWindow);
			window.setVisibility(View.GONE);
		}
		
	};
	
	int[] getHighScores() {
		return ((RunnerGame)game).getScores();
	}
	
	public GameScreen(Game game) {
		super(game);
		
		sharkAnim = new Animation((Sprite)ResourceManager.get("Game", "Shark"));
		sharkX = -((Sprite)ResourceManager.get("Game", "Shark")).getWidth() * 3;
		
		StickMan.runSpeed = 0;
		stickMan = new StickMan(this, game);
		
		((Music)ResourceManager.get("Game", "DeathMusic")).stop();
		
		env.add(new Line(0, 700, 2000, 0, true));
		lastGroundEnd = 2000;
		
		if (getHighScores()[0] == -1) {
			inTut = 0;
			
			generateTutTerrain();
			lastCeilingEnd += minWidth * 21;
		} else {
			int width = game.getGraphics().getWidth();
			
			while (drawPosX < width) {
				generateRandomShape();
				
				updateProbabilities();
			}
		}
		
		((Sound)ResourceManager.get("Sounds", "drawingStart")).play(1);
		
		layout = this.getLayoutFromId(R.layout.game_gui);

		ImageButton quitButton = (ImageButton)layout.findViewById(R.id.quitButton);
		quitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ResourceManager.pauseAllSounds();
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.QuitWindow);
				window.setVisibility(View.VISIBLE);
				
				window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
				window.setVisibility(View.GONE);
				softPause = true;
			}
		});

		ImageButton pauseButton = (ImageButton)layout.findViewById(R.id.pauseButton);
		pauseButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (softPause)
					return;
				
				ResourceManager.pauseAllSounds();
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
				window.setVisibility(View.VISIBLE);
				
				TextView scoreView = (TextView)layout.findViewById(R.id.pauseScoreText);
				scoreView.setText(String.valueOf(getCurrentScore()));
				
				wasInPause = true;
				softPause = true;
			}
		});

		ImageButton resumeButton = (ImageButton)layout.findViewById(R.id.resumeButton);
		resumeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
				window.setVisibility(View.GONE);
				wasInPause = false;
				softPause = false;
				
				if (inTut == -1 || tutTimer > 0)
					ResourceManager.resumeAllSounds();
			}
		});
		
		ImageButton quitAccept = (ImageButton)layout.findViewById(R.id.quitAccept);
		quitAccept.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				goToMainMenu();
			}
		});
		
		ImageButton quitCancel = (ImageButton)layout.findViewById(R.id.quitCancel);
		quitCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.QuitWindow);
				window.setVisibility(View.GONE);
				
				if (!wasInPause) {
					softPause = false;
					ResourceManager.resumeAllSounds();
					return;
				}
				
				window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
				window.setVisibility(View.VISIBLE);
			}
		});
		
		ImageButton playAgain = (ImageButton)layout.findViewById(R.id.playAgainButton);
		playAgain.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				playAgain();
			}
		});
		
		ImageButton playAgainCancel = (ImageButton)layout.findViewById(R.id.mainMenuButton);
		playAgainCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				goToMainMenu();
			}
		});
		
		SeekBar musicBar = (SeekBar)layout.findViewById(R.id.MusicBarGame);
		musicBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if (arg2)
					setMusic(arg1 / 100f);
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		musicBar.setProgress((int)(game.getAudio().getMusicVolume() * 100));
		
		SeekBar soundBar = (SeekBar)layout.findViewById(R.id.SoundBarGame);
		soundBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if (arg2)
					setSound(arg1 / 100f);
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		soundBar.setProgress((int)(game.getAudio().getSoundVolume() * 100));
		
		ImageButton openScoreView = (ImageButton)layout.findViewById(R.id.showScoreButton);
		openScoreView.setOnClickListener(scoreButtonListener);
		
		openScoreView = (ImageButton)layout.findViewById(R.id.showScoreButtonPaused);
		openScoreView.setOnClickListener(scoreButtonListener);
		
		ImageButton closeScoreView = (ImageButton)layout.findViewById(R.id.closeScoreWindowButton);
		closeScoreView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				RelativeLayout HighScoreView = (RelativeLayout)layout.findViewById(R.id.HighScoresWindow);
				HighScoreView.setVisibility(View.GONE);
				
				if (wasInPause) {
					RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
					window.setVisibility(View.VISIBLE);
				} else {
					RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.ScoreWindow);
					window.setVisibility(View.VISIBLE);
				}
			}
			
		});
		
		Button connectButton = (Button)layout.findViewById(R.id.gameViewLeaderboard);
		if (((RunnerGame)game).getConnectToGoogle() && !game.getApiClient().isConnected()) {
			game.attemptConnectToGooglePlayService();
		} else if (game.getApiClient().isConnected())
			updateGooglePlayGUI();
		
		connectButton.setOnClickListener(new OnClickListener() {
	
			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);
				
				displayLeaderboard();
			}
			
		});
		
		Button achievementButton = (Button)layout.findViewById(R.id.gameViewAchievements);
		
		achievementButton.setOnClickListener(new OnClickListener() {
	
			@Override
			public void onClick(View arg0) {
				((Sound)ResourceManager.get("Sounds", "click")).play(1);

				displayAchievements();
			}
			
		});
		
		game.setGuiLayer(layout);
	}
	
	protected void toggleGooglePlayConnection() {

		if (game.getApiClient().isConnected()) {
			displayLeaderboard();
		} else {
			game.attemptConnectToGooglePlayService();
		}
	}
	
	public void updateGooglePlayGUI() {

		game.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Button connectButton = (Button)layout.findViewById(R.id.gameViewLeaderboard);
				Button achievementButton = (Button)layout.findViewById(R.id.gameViewAchievements);
				
				if (game.getApiClient().isConnected()) {
					connectButton.setText("View Leaderboard");
					achievementButton.setText("Achievements");
				} else {
					connectButton.setText("Connect to Leaderboard");
					achievementButton.setText("Connect to Achievements");
				}
				
			}
			
		});
	}

	void displayLeaderboard() {

		if (game.getApiClient().isConnected()) {
			game.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(game.getApiClient(), game.getString(R.string.leaderboard_high_scores)), 0);
		} else {
			game.attemptConnectToGooglePlayService();
		}
	}
	
	void displayAchievements() {
		if (game.getApiClient().isConnected())
			game.startActivityForResult(Games.Achievements.getAchievementsIntent(game.getApiClient()), 0);
		else
			game.attemptConnectToGooglePlayService();
	}
	
	private void generateTutTerrain() {
		
		int x = lastGroundEnd;
		
		spawnCollectables(x - minWidth * 3, maxDrawSpace, minWidth * 2, 0);
		
		// Create pit
		env.add(new Line(x, maxDrawSpace, 0, 500, true));
		x += minWidth * 2;
		env.add(new Line(x, maxDrawSpace, 0, 500, true));
		env.add(new Line(x, maxDrawSpace, minWidth * 19, 0, true));
		lastGroundEnd = x + minWidth * 19;
		
		x += minWidth * 5;
		
		// Create downward ramp, aligned with the floor
		drawRamp(x, maxDrawSpace - zoneHeight, minWidth, zoneHeight, false);
		x += minWidth * 8;
		
		// Create small tunnel w/ box at the end
		env.add(new Line(x, maxDrawSpace - zoneHeight, minWidth * 3, 0, true));
		x += minWidth * 3;
		drawBox(x, maxDrawSpace - zoneHeight, minWidth / 2, (zoneHeight / 2));
		
		drawPosX = x + minWidth * 4;
		tutTimer = .001f;
	}

	void setMusic(float music) {
		game.getAudio().setMasterVolume(music);
	}
	
	void setSound(float sound) {
		game.getAudio().setMasterVolumeSounds(sound);
	}
	
	void playAgain() {
		game.setScreen(new GameScreen(game));
	}
	
	void updateScore() {
		((RunnerGame)game).addScore(getCurrentScore());
		
		softPause = true;
	}
	
	void showScore() {
		
		game.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.ScoreWindow);
		
				window.setVisibility(RelativeLayout.VISIBLE);
		
				TextView scoreView = (TextView)layout.findViewById(R.id.scoreView);
				scoreView.setText(String.valueOf(getCurrentScore()));
				
				scoreView = (TextView)layout.findViewById(R.id.runningScore);
				scoreView.setVisibility(View.GONE);
				
				scoreView = (TextView)layout.findViewById(R.id.textView3);
				scoreView.setVisibility(View.GONE);
			}
		});
	}
	
	void spawnCollectables(int x, int y, int width, float slope) {
		double angle = Math.atan(slope);
		int xAdjust = (int)(Math.sin(angle) * Collectable.height / 2) - Collectable.width / 2;
		int yAdjust = -(int)(Math.cos(angle) * Collectable.height / 2) - Collectable.height / 2;
		
		for (int i = Collectable.width / 2; i < width; i += Collectable.width) {
			Collectable c = new Collectable(x + i + xAdjust, y + (int)(i * slope) + yAdjust, totalDistanceTraveled);
			
			if (slope == 0) {
				boolean hit = false;
				for (Collidable col : env)
					if (Rect.intersects(c.getBounds(), col.getBounds())) {
						hit = true;
						break;
					}
				if (!hit)
					env.add(c);
			} else {
				env.add(c);
			}
		}
	}
	
	void generateRandomShape () {
		int x = drawPosX;
		int y = drawPosY;
		
		while (y > maxDrawSpace) {
			y -= (zoneHeight);
		} 
		while (y < minDrawSpace) {
			y += (zoneHeight);
		}
		
		int choice = rand.nextInt(totalProbabilityCount());
		
		int threshold = gap;
		
		int range = totalDistanceTraveled / distancePerDifficulty >= 6 ?
				(int)((1 - (totalDistanceTraveled - 6 * distancePerDifficulty) / (2f * distancePerDifficulty)) * minWidth) : minWidth;
		
		if (range < 1)
			range = 1;
		
		int width = rand.nextInt(range) + minWidth;
		int height = (zoneHeight) * (rand.nextInt(1) + 1);//rand.nextInt(StickMan.bottomSafeRange * 2) + StickMan.bottomSafeRange * 4 / 3;
		
		boolean spawnCollectables = rand.nextInt(2) == 0;
		
		if (choice < threshold) {

			height = 0;
			width *= 2;
			gap = -1;
		
		} else if (choice < (threshold += drop)) {

			while (y + height > maxDrawSpace)
				height -= (zoneHeight);
			width = 0;
			drop = -1;
			
		} else if (choice < (threshold += ground)) {
			
			drawGround(x, y, width, spawnCollectables);
			height = 0;
			ground = -1;
			
		} else if (choice < (threshold += platform)) {
			
			drawPlatform(x, y, width);
			height = 0;
			platform = -1;
			
		} else if (choice < (threshold += jump)) {

			width = 0;
			height *= -1;
			
			while (y + height < minDrawSpace)
				height += (zoneHeight);
			jump = -1;
			
		} else if (choice < (threshold += wall)) {
			
			if (width % 2 == 0 && y + height > maxDrawSpace && y - height > minDrawSpace) {
				height *= -1;
			
				while (y + height < minDrawSpace)
					y += (zoneHeight);
			} else
				while (y + height > maxDrawSpace)
					y -= (zoneHeight);
			
			boolean shortWall = false;
			//if (rand.nextInt(2) == 0)
				shortWall = true;
			
			drawWall(x, y, height / (shortWall ? 2 : 1));
			width = 0;
			wall = -1;
			box = -1;
			
		} else if (choice < (threshold += box)) {
			
			//height *= -.5f;
			width = height;
			if (width % 2 == 0)
				while (y - height < minDrawSpace)
					y += (zoneHeight);
			else
				while (y + height > maxDrawSpace)
					y -= (zoneHeight);
			
			drawBox(x, y, width / 2, (height / 2) * (width % 2 == 0 ? -1 : 1));
			box = -1;
			wall = -1;
			width -= width / 2;
			
		} else if (choice < (threshold += ramp)) {
			
			if (width % 2 == 0 && y + height > maxDrawSpace && y - height > minDrawSpace) {
				height *= -1;
				
				while (y + height < minDrawSpace)
					y += (zoneHeight);
			}
			while (y + height > maxDrawSpace)
				y -= (zoneHeight);
			
			//if (height > width * 2f / 3)
			//	height = (int)(width * 2f / 3);
			//else if (height < -width * 2f / 3)
			//	height = -(int)(width * 2f / 3);
			
			drawRamp(x, y, width, height, spawnCollectables);
			width = width + minWidth * 3;
			ramp = -1;
			
		} else if (choice < (threshold += walledRamp)) {
			
			while (y - height < minDrawSpace)
				y += (zoneHeight);
			
			if (width % 2 == 0) {
				height *= -1;
			}

			//if (height > width * 2f / 3)
			//	height = (int)(width * 2f / 3);
			//else if (height < -width * 2f / 3)
			//	height = -(int)(width * 2f / 3);
			
			//while (y - Math.abs(height) < minDrawSpace)
			//	height += -(height / Math.abs(height)) * (zoneHeight);
			
			drawWalledRamp(x, y, width, height, true, spawnCollectables);
			//width = width + minWidth * 3;
			x += height > 0 ? minWidth : minWidth / 2;
			walledRamp = -1;
			rampPit = -1;
			
		} else if (choice < (threshold += rampPit)) {
			
			int pitWidth = rand.nextInt(minWidth * 2) + minWidth;
			height *= -1;
			
			while (y + height < minDrawSpace)
				y += (zoneHeight);
			
			//if (height < -width * 2f / 3)
			//	height = -(int)(width * 2f / 3);
			
			drawRampPit(x, y, width, height, pitWidth, spawnCollectables);
			
			width = width * 2 + pitWidth + minWidth / 2;
			rampPit = -1;
			walledRamp = -1;
			
		} else if (choice < (threshold += tunnel)) {
			width *= 4;
			if (upsideDownTunnel) {
				width += minWidth;
				drawGround(x + minWidth, 700 - (zoneHeight), width - 2 * minWidth, spawnCollectables);
				drawGround(x, 700 - 2 * (zoneHeight), width, false);
				height = 700 - (zoneHeight) - y;
			} else {
				drawGround(x, 700 - (zoneHeight), width, spawnCollectables);
				drawGround(x + minWidth, 700 - 2 * (zoneHeight), width - 2 * minWidth, false);
				height = 700 - 2 * (zoneHeight) - y;
			}
			upsideDownTunnel = !upsideDownTunnel;
			tunnel = -1;
		} else if (choice < (threshold += hole)) {
			width = minWidth * 3;

			// Ceiling
			env.add(new Line(x - 900, minDrawSpace - 1000, 900, 1000, true));
			env.add(new Line(x, minDrawSpace, width, 0, true));
			env.add(new Line(x + width, minDrawSpace, 900, -1000, true));
			
			height = rand.nextInt(maxDrawSpace - minDrawSpace - zoneHeight * 2 / 3);
			x += width / 2;
			
			// Wall with hole
			env.add(new Line(x, minDrawSpace, 0, height, true));
			y = minDrawSpace + height + zoneHeight * 2 / 3;
			env.add(new Line(x, y, 0, maxDrawSpace - y, true));
			
			x += minWidth * 2;
			
			if (y > maxDrawSpace - zoneHeight)
				y = maxDrawSpace;
			else if (y > maxDrawSpace - zoneHeight * 2)
				y = maxDrawSpace - zoneHeight;
			else if (y > maxDrawSpace - zoneHeight * 3)
				y = maxDrawSpace - zoneHeight * 2;
			
			hole = -1;
			width = 0;
			height = 0;
		}

		drawPosX = x + width;
		drawPosY = y + height;
	}
	
	// clears any pencils colliding with the last shape found in env
	void clearCollidingPencils() {
		Rect bounds = env.get(env.size() - 1).getBounds();
		
		for (int i = 0; i < env.size(); i++) {
			Collidable c = env.get(i);
			if (c.getClass().getName() != Collectable.class.getName())
				continue;
			
			Rect boundsOther = c.getBounds();
			
			if (Rect.intersects(bounds, boundsOther)) {
				env.remove(i);
				i--;
			}
		}
	}
	
	void drawWall(int x, int y, int height) {
		env.add(new Line(x, y, 0, height, true));
		clearCollidingPencils();
	}
	
	void drawGround(int x, int y, int width, boolean collectables) {
		if (y ==  700)
			return;
		
		env.add(new Line(x, y, width, 0, true));
		if (collectables) {
			spawnCollectables(x, y, width, 0);
		}
	}
	
	void drawPlatform(int x, int y, int width) {
		if (y == 700)
			return;
		
		env.add(new ThinLine(x, y, width, 0, true));
	}
	
	void drawBox(int x, int y, int width, int height) {
		env.add(new Line(x, y, 0, height, true));
		clearCollidingPencils();
		env.add(new Line(x, y + height, width, 0, true));
		env.add(new Line(x + width, y + height, 0, -height, true));
		clearCollidingPencils();
		env.add(new Line(x, y, width, 0, true));
		clearCollidingPencils();
	}
	
	void drawRamp(int x, int y, int width, int height, boolean collectables) {
		x += minWidth * 2;
		env.add(new Line(x, y, minWidth, 0, true));
		
		x += minWidth;
		env.add(new Line(x, y, width, height, true));
		clearCollidingPencils();
		
		if (collectables) {
			float slope = (float)height / width;
			spawnCollectables(x, y, width, slope);
		}
	}
	
	void drawWalledRamp(int x, int y, int width, int height, boolean safety, boolean collectables) {
		
		if (safety) {
			//x += minWidth;
			env.add(new Line(x, y, height > 0 ? minWidth : minWidth / 2, 0, true));
			x += height > 0 ? minWidth : minWidth / 2;
		}
		
		env.add(new Line(x, y, width, 0, true));
		
		if (height > 0) {
			env.add(new Line(x, y, 0, -height, true));
		}
		
		env.add(new Line(x, y + (height > 0 ? -height : 0), width, height, true));
		clearCollidingPencils();
		
		if (collectables) {
			float slope = (float)height / width;
			spawnCollectables(x, y + (height > 0 ? -height : 0), width, slope);
		}
		
		if (height < 0) {
			env.add(new Line(x + width, y + height, 0, -height, true));
		}
	}
	
	void drawRampPit(int x, int y, int rampWidth, int rampHeight, int pitWidth, boolean collectables) {
		drawWalledRamp(x, y, rampWidth, rampHeight, true, collectables);
		drawWalledRamp(x + minWidth / 2 + rampWidth + pitWidth, y, rampWidth, -rampHeight, false, collectables);
	}
	
	void playDeathSong() {
		((Music)ResourceManager.get("Game", "BackgroundMusic")).stop();
		((Music)ResourceManager.get("Game", "DeathMusic")).play();
		//((Music)ResourceManager.get("Game", "DeathMusic")).seekBegin();
	}
	
	float timeSinceLastUpdate = 0;
	
	@Override
	public void update(float deltaTime) {
		
		if (softPause) {

			int volume = (int)(game.getAudio().getMusicVolume() * 100 * (game.getAudio().getAudioFocus() == Audio.AudioFocus.DUCK ? 2 : 1));
			SeekBar musicBar = (SeekBar)layout.findViewById(R.id.MusicBarGame);
			if (volume != musicBar.getProgress() && game.getAudio().getAudioFocus() != Audio.AudioFocus.LOST) {
				game.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						int volume = (int)(game.getAudio().getMusicVolume() * 100 * (game.getAudio().getAudioFocus() == Audio.AudioFocus.DUCK ? 2 : 1));
						SeekBar musicBar = (SeekBar)layout.findViewById(R.id.MusicBarGame);
						musicBar.setProgress(volume);
					}
				});
			}
			
			volume = (int)(game.getAudio().getSoundVolume() * 100 * (game.getAudio().getAudioFocus() == Audio.AudioFocus.DUCK ? 2 : 1));
			SeekBar soundBar = (SeekBar)layout.findViewById(R.id.SoundBarGame);
			if (volume != soundBar.getProgress() && game.getAudio().getAudioFocus() != Audio.AudioFocus.LOST) {
				game.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						int volume = (int)(game.getAudio().getSoundVolume() * 100 * (game.getAudio().getAudioFocus() == Audio.AudioFocus.DUCK ? 2 : 1));
						SeekBar soundBar = (SeekBar)layout.findViewById(R.id.SoundBarGame);
						soundBar.setProgress(volume);
					}
				});
			}
			
			return;
		} else if (lastScore != getCurrentScore()) {
			lastScore = getCurrentScore();
			game.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					TextView score = (TextView)layout.findViewById(R.id.runningScore);
					score.setText(String.valueOf(getCurrentScore()));
				}
				
			});
		}
		
		if (timeSinceLastUpdate > 1f / 60) {
			
			if (timeSinceLastUpdate > 1)
				timeSinceLastUpdate = 1;
			
			timeSinceLastUpdate -= 1f / 60;
			update(deltaTime); // check for catch up updates
			deltaTime = 1f / 60; // make constant time step
		} else {
			timeSinceLastUpdate += deltaTime;
			return;
		}

		if (!stickMan.alive)
			return;
		
		
		if (drawingEnvironment) {
			StickMan.runSpeed = 0;
			return;
		}
		
		if (initializing) {
			drawTime += deltaTime;
			
			if (drawTime > .8) {
				((Sound)ResourceManager.get("Sounds", "drawingStart")).stopAll();
				((Music)ResourceManager.get("Game", "BackgroundMusic")).play();
			}
			
			if (drawTime > 1.9) { // Start accelerating!
				StickMan.runSpeed += StickMan.maxRunSpeed * 2 * deltaTime;
				if (StickMan.runSpeed > StickMan.maxRunSpeed)
					StickMan.runSpeed = StickMan.maxRunSpeed;
			}
			
			if (drawTime > 2 && !playedFootstepSound) {
				((Sound)ResourceManager.get("Sounds", "footsteps")).play(1);
				playedFootstepSound = true;
			}
			
			if (!stickMan.isSpawning()) {
				initializing = false;
				StickMan.runSpeed = StickMan.maxRunSpeed;
			}
		}
		
		if (!initializing && inTut >= 0 && tutTimer > 0) {
			tutTimer -= deltaTime;
			if (tutTimer <= 0) {
				inTut++;
				exampleTimer = 0;
				ResourceManager.pauseAllSounds();
			}
		}
		
		if (!initializing && (inTut == -1 || tutTimer <= 0))
			handleInput(deltaTime);
		
		if (inTut == -1 || tutTimer > 0) {
		
			lastGroundEnd = lastGroundEnd - (int)(StickMan.runSpeed * deltaTime);
			lastCeilingEnd = lastCeilingEnd - (int)(StickMan.runSpeed * deltaTime);
			drawPosX = drawPosX - (int)(StickMan.runSpeed * deltaTime);
			
			if (inTut == -1)
				totalDistanceTraveled += StickMan.runSpeed * deltaTime;
			
			if ((int)(totalDistanceTraveled / 155) == 500 && game.getApiClient().isConnected())
				Games.Achievements.unlock(game.getApiClient(), game.getString(R.string.achievement_endurant));
			if ((int)(totalDistanceTraveled / 155) == 2000 && game.getApiClient().isConnected())
				Games.Achievements.unlock(game.getApiClient(), game.getString(R.string.achievement_cross_country_runner));
		
			stickMan.update(deltaTime);
		
			Collectable.updateAnimStart(deltaTime);
			
			if (stickMan.getYPosition() > 800 + stickMan.getHeight()) {
				stickMan.die();
				playDeathSong();
				updateScore();
				return;
			}
			
			Rect stickManBounds = stickMan.getBounds();
			
			boolean onGround = false;
			for (int i = 0; i < env.size(); i++) {
				Collidable c = env.get(i);
				c.update(deltaTime);
				
				if (!stickMan.alive)
					continue;
				
				if (initializing) {
					stickMan.setHeight(700);
					onGround = true;
					continue;
				}
				
				Rect bounds = c.getBounds();
				if (Rect.intersects(bounds, stickManBounds)) {
					onGround = c.collide(stickMan) || onGround;
					if (!stickMan.alive){
						updateScore();
						playDeathSong();
						continue;
					}
				} else if (bounds.right < 0) {
					env.remove(c);
					i--;
				}
			}
			stickMan.setOnGround(onGround);
		}
		
		if (inTut != -1)
			stickMan.collectionCount = 0;
		
		int generateThreshold = game.getGraphics().getWidth() + StickMan.runSpeed;
		
		if (lastGroundEnd < generateThreshold) {
			if (!drewPitLast && rand.nextInt(10 - (int)clamp((int)(totalDistanceTraveled / 5000), 0, 7)) == 0) {
				int pitWidth = rand.nextInt(minWidth * 3) + minWidth;
				if (drawPosX > lastGroundEnd) {
					env.add(new Line(lastGroundEnd, maxDrawSpace, drawPosX - lastGroundEnd, 0, true));
					lastGroundEnd = drawPosX;
				}
				env.add(new Line(lastGroundEnd, maxDrawSpace, 0, 100, true));
				env.add(new Line(lastGroundEnd + pitWidth, maxDrawSpace, 0, 100, true));
				lastGroundEnd += pitWidth;
				drewPitLast = true;
				drawPosY -= (zoneHeight);
			} else {
				int width = rand.nextInt(500) + 500;
				env.add(new Line(lastGroundEnd, maxDrawSpace, width, 0, true));
				
				if (rand.nextInt(6) == 0) {
					spawnCollectables(lastGroundEnd, maxDrawSpace, width, 0);
				}
				
				lastGroundEnd += width;
				drewPitLast = false;
			}
		}
		
		if (lastCeilingEnd < generateThreshold
			&& (rand.nextInt(10000 - 1000 * (int)clamp((int)(totalDistanceTraveled / 10000), 0, 8)) == 0 || getCurrentScore() >= 500 && getCurrentScore() <= 515)) {
			
			env.add(new Line(generateThreshold, minDrawSpace - 1000, 900, 1000, true));
			int width = rand.nextInt(10000) + 5000;
			env.add(new Line(generateThreshold + 900, minDrawSpace, width, 0, true));
			env.add(new Line(generateThreshold + 900 + width, minDrawSpace, 900, -1000, true));
			
			lastCeilingEnd = generateThreshold + 1800 + width;
		}
		
		if (drawPosX < generateThreshold) {
			generateRandomShape();
		
			updateProbabilities();
		}
	}
	
	void handleInput(float deltaTime) {
		Input in = game.getInput();
		
		if (inTut == -1)
			for (Line l : currentDraw) {
				l.update(deltaTime);
			}
		
		if (hasFirstTouch)
			firstTouchX = firstTouchX - (int)(deltaTime * StickMan.runSpeed);
		
		if (currentPointer == -1) {
			List<TouchEvent> touches = in.getTouchEvents();
			
			for (TouchEvent t : touches) {
				if (t.type != TouchEvent.TOUCH_UP) {
					currentPointer = t.pointer;
					break;
				}
			}
			
			if (currentPointer == -1)
				return;
		}
		
		if (!in.isTouchDown(currentPointer)) {
			currentPointer = -1;
			((Sound)ResourceManager.get("Sounds", "drawing")).stopAll();
			
			if (currentDraw.size() == 0) {
				hasFirstTouch = false;
				firstTouchX = -1;
				firstTouchY = -1;
				
				return;
			}
			
			int switchBackCount = 0;
			int vertSwitchBack = 0;
			
			int leftMostX = game.getGraphics().getWidth();
			int rightMostX = 0;
			int topMostY = 800;
			int bottomMostY = 0;
			
			boolean currentDirectionIsRight = currentDraw.get(0).getX2() > 0;
			boolean currentDirectionIsUp = currentDraw.get(0).getY2() < 0;
			
			// Analyze Lines
			for (Line l : currentDraw) {
				int x1 = l.getX1();
				int x2 = l.getX1() + l.getX2();
				int y1 = l.getY1();
				int y2 = l.getY1() + l.getY2();
				
				if (x1 < leftMostX)
					leftMostX = x1;
				else if (x1 > rightMostX)
					rightMostX = x1;
				
				if (x2 < leftMostX)
					leftMostX = x2;
				else if (x2 > rightMostX)
					rightMostX = x2;
				
				if (y1 < topMostY)
					topMostY = y1;
				else if (y1 > bottomMostY)
					bottomMostY = y1;
				
				if (y2 < topMostY)
					topMostY = y2;
				else if (y2 > bottomMostY)
					bottomMostY = y2;
				
				if (currentDirectionIsUp && l.getY2() < 0) {
					currentDirectionIsUp = false;
					vertSwitchBack++;
				} else if (!currentDirectionIsUp && l.getY2() > 0) {
					currentDirectionIsUp = true;
					vertSwitchBack++;
				}
				
				if (currentDirectionIsRight && l.getX2() < 0) {
					currentDirectionIsRight = false;
					switchBackCount++;
				} else if (!currentDirectionIsRight && l.getX2() > 0) {
					currentDirectionIsRight = true;
					switchBackCount++;
				}
			}
			
			double vertDiff = Math.abs(currentDraw.get(0).getY1() - (currentDraw.get(currentDraw.size() - 1).getY1() + currentDraw.get(currentDraw.size() - 1).getY2()));

			boolean validDraw = true;
			// Check lines against against tutorial space
			switch (inTut) {
			case 2:
				validDraw = (leftMostX < minWidth + StickMan.width / 2
							&& rightMostX > minWidth * 3 - StickMan.width / 2
							&& topMostY > maxDrawSpace - StickMan.height / 2
							&& bottomMostY < maxDrawSpace + StickMan.height / 2);
				break;
			case 4:
				validDraw = (leftMostX > minWidth && leftMostX < minWidth * 2.5 - StickMan.width
							&& rightMostX < minWidth * 2.5
							&& topMostY > maxDrawSpace - StickMan.height / 2 && topMostY < maxDrawSpace - StickMan.height / 4
							&& bottomMostY < maxDrawSpace + StickMan.height / 4);
				break;
			case 6:
				validDraw = (leftMostX > minWidth + StickMan.width / 2
							&& rightMostX < minWidth * 2
							&& topMostY < maxDrawSpace
							&& bottomMostY > maxDrawSpace - StickMan.height / 2);
				break;
			default: break;
			}
			
			if (validDraw) {
				 if ((inTut == -1 || inTut == 6) && vertSwitchBack > 0 && vertDiff < (bottomMostY - topMostY) / 2 && rightMostX - leftMostX < (bottomMostY - topMostY) * 3 / 2) {
					
					int height = (bottomMostY - topMostY);
					int y = topMostY - height / 2;
					height *= 3f / 2;
					env.add(0, new Banana(leftMostX, y, rightMostX - leftMostX, height));
					
					if (inTut != -1) {
						tutTimer = (minWidth * 4f) / StickMan.maxRunSpeed;
						ResourceManager.resumeAllSounds();
					}
				} else if ((inTut == -1 || inTut == 4) && switchBackCount >= 2 && bottomMostY - topMostY < rightMostX - leftMostX) {
					int width = (rightMostX - leftMostX) * 2 / 3;
					int totalSpace = (rightMostX - leftMostX);
					int x = leftMostX + (totalSpace - width) / 2;
					env.add(0, new BouncePad(x, topMostY, width, bottomMostY - topMostY));
					
					if (inTut != -1) {
						tutTimer = (minWidth * 2f) / StickMan.maxRunSpeed;
						ResourceManager.resumeAllSounds();
					}
				} else if (inTut == -1 || inTut == 2) {
					env.addAll(0, currentDraw);
					
					if (inTut != -1) {
						tutTimer = (minWidth * 3f) / StickMan.maxRunSpeed;
						ResourceManager.resumeAllSounds();
					}
				} else if (inTut == 1) {
					tutTimer = (float)(2000 - minWidth) / StickMan.maxRunSpeed;
					ResourceManager.resumeAllSounds();
				} else if (inTut == 3) {
					tutTimer = (minWidth * 4.75f) / StickMan.maxRunSpeed;
					ResourceManager.resumeAllSounds();
				} else if (inTut == 5) {
					tutTimer = (minWidth * 7f) / StickMan.maxRunSpeed;
					ResourceManager.resumeAllSounds();
				} else if (inTut == 7) {
					inTut = -1;
					ResourceManager.resumeAllSounds();
				}
			}
			
			currentDraw.clear();
			hasFirstTouch = false;
			firstTouchX = -1;
			firstTouchY = -1;
			
			return;
		}
		
		if (currentDraw.size() == 0 && !hasFirstTouch) {
			hasFirstTouch = true;
			firstTouchX = in.getTouchX(currentPointer);
			firstTouchY = in.getTouchY(currentPointer);
			((Sound)ResourceManager.get("Sounds", "drawing")).play(1);
			return;
		} else if (currentDraw.size() == 0 && hasFirstTouch) {
			currentDraw.add(new Line(firstTouchX, firstTouchY,
					in.getTouchX(currentPointer) - firstTouchX,
					in.getTouchY(currentPointer) - firstTouchY, false));
			return;
		}
		
		Line lastLine = currentDraw.get(currentDraw.size() - 1);
	
		int lastEndX = lastLine.getX1() + lastLine.getX2();
		int lastEndY = lastLine.getY1() + lastLine.getY2();
		
		int pointerX = in.getTouchX(currentPointer);
		int pointerY = in.getTouchY(currentPointer);
		
		float distance = (float)Math.sqrt(Math.pow(pointerX - lastEndX, 2) + Math.pow(pointerY - lastEndY, 2));
		
		if (distance > 50 && pointerX > 0 && pointerX < game.getGraphics().getWidth()
				&& pointerY > 0 && pointerY < game.getGraphics().getHeight()) {
			currentDraw.add(new Line(lastEndX, lastEndY, pointerX - lastEndX, pointerY - lastEndY, false));
		}
	}

	@Override
	public void paint(float deltaTime) {

		Graphics g = game.getGraphics();
		Image bg = (Image)ResourceManager.get("Game", "Background");
		g.drawImage(bg, screenWidth / 2 - bg.getWidth() / 2, 0);

		if (drawingEnvironment) {
			if (env.size() == 0 || inTut != -1 && envAnimIndex > 0) { // fail-safe
				drawingEnvironment = false;
				return;
			}
			
			drawTime += deltaTime;
			
			for (int i = 0; i < envAnimIndex; i++)
				env.get(i).paint(deltaTime, g);
			
			Collidable current = env.get(envAnimIndex);
			float newTimePerLine = current.getClass().getName() == Collectable.class.getName() ? .1f : timePerLine;
			
			if (drawTime / newTimePerLine < 1) {
				current.paintPartial(drawTime / newTimePerLine, g);
			}
			else {
				current.paint(deltaTime, g);
				envAnimIndex++;
				drawTime = 0;
			}
					
			if (envAnimIndex == env.size()) {
				drawingEnvironment = false;
			}
			return;
		}

		stickMan.paint(deltaTime, g);
		
		for (Collidable c : env) {
			Rect bounds = c.getBounds();
			if (!onScreen(bounds, g))
				continue;
			
			c.paint(deltaTime, g);
		}
		
		if (inTut > 0 && tutTimer <= 0) {
			drawInstructions(g, deltaTime);
		}
		
		for (Line l : currentDraw) {
			l.drawingPaint(deltaTime, g);
		}
		
		if (!stickMan.alive) {
			sharkAnim.update(deltaTime);
			sharkTime += deltaTime;
			float amplitude = ((float)Math.cos(sharkTime * 3 * Math.PI) + 1) / 2;
			float speed = (float)(screenWidth * amplitude);
			sharkX += speed * deltaTime;
			
			if (amplitude < .5 && !chompPlayed && sharkX < screenWidth) {
				chompPlayed = true;
				((Sound)ResourceManager.get("Sounds", "chomp")).play(1);
			} else if (amplitude > .5) {
				chompPlayed = false;
				((Sound)ResourceManager.get("Sounds", "chomp")).stopAll();
			}
			
			Sprite sprite = (Sprite)ResourceManager.get("Game", "Shark");
			int width = sprite.getWidth() * 3;
			int height = sprite.getHeight() * 3;
			
			g.drawScaledImage(bg, screenWidth / 2 - bg.getWidth() / 2, 0, sharkX + width < bg.getWidth() ? sharkX + width : bg.getWidth(), screenHeight,
					0, 0, sharkX + width < bg.getWidth() ? sharkX + width : bg.getWidth(), bg.getHeight());
			sharkAnim.paint(g, sharkX, 0, width, height);
			
			if (sharkX + width > screenWidth + 100 && !scoreShown) {
				showScore();
				scoreShown = true;
			}
		}
		
		/*fpsTimer -= deltaTime;
		
		if (fpsTimer <=0 ) {
			fps = fpsCount;
			fpsCount = 0;
			fpsTimer = 1;
		} else
			fpsCount++;
		
		Paint p = new Paint();
		p.setTextSize(50);
		p.setColor(Color.parseColor("#0909ff"));
		
		g.drawString("FPS: " + fps, screenWidth - 400, 100, p);*/
	}
	// TEMP
	//int fps;
	//int fpsCount = 0;
	//float fpsTimer = 1;
	//End TEMP

	float exampleTimer = 0;
	
	void drawInstructions(Graphics g, float deltaTime) {
		g.drawRect(0, 0, screenWidth, screenHeight, transparentBlack);
		
		exampleTimer += deltaTime;
		float percent = clamp(exampleTimer * 2, 0, 1);
		
		if (exampleTimer > 5)
			exampleTimer = 0;
		
		int alpha = (int)(250 * (Math.sin(exampleTimer * Math.PI) / 4 + .75));
		String color = "#" + Integer.toHexString(alpha) + (alpha < 16 ? "0" : "") + "C0C010";
		
		Paint p = new Paint();
		p.setTextSize(75);
		p.setColor(Color.parseColor("#FFFFFF"));
		
		int y = screenHeight / 4;
		
		switch(inTut) {
		case 1:
			drawShadowedString("Welcome to Sketch Runner.", y, p, g);
			drawShadowedString("This tutorial will help", y + 150, p, g);
			drawShadowedString("you learn gameplay basics.", y + 225, p, g);
			drawShadowedString("Distance and pencils will", y + 375, p, g);
			drawShadowedString("increase your score.", y + 450, p, g);
			break;
		case 2:
			drawShadowedString("Draw lines to get past", y, p, g);
			drawShadowedString("obsticles.", y + 75, p, g);
			drawShadowedString("Lines aren't solid until", y + 225, p, g);
			drawShadowedString("you stop drawing.", y + 300, p, g);
			
			int x = minWidth + StickMan.width / 3;
			g.drawLine(x, maxDrawSpace, x + (int)(percent * (minWidth * 2 - StickMan.width * 2f / 3)), maxDrawSpace, Color.parseColor(color), 10);
			break;
		case 3:
			drawShadowedString("Lines can be straight,", y, p, g);
			drawShadowedString("curved, at an angle, long,", y + 75, p, g);
			drawShadowedString("short -- anything you can", y + 150, p, g);
			drawShadowedString("think of to survive.", y + 225, p, g);
			break;
		case 4:
			drawShadowedString("Draw a bounce pad to", y, p, g);
			drawShadowedString("move upward quickly.", y + 75, p, g);
			drawShadowedString("Using bounce pads will", y + 225, p, g);
			drawShadowedString("get you extra points.", y + 300, p, g);
			
			float p1 = clamp(percent, 0, 1 / 3f) * 3;
			float p2 = (clamp(percent, 1 / 3f, 2 / 3f) - 1 / 3f) * 3;
			float p3 = (clamp(percent, 2 / 3f, 1) - 2 / 3f) * 3;
			
			g.drawLine((int)(minWidth * 1.5), maxDrawSpace - StickMan.height / 3, (int)(minWidth * 1.5 + p1 * minWidth * .75), maxDrawSpace - StickMan.height / 3, Color.parseColor(color), 10);
			g.drawLine((int)(minWidth * 2.25), maxDrawSpace - StickMan.height / 3, (int)(minWidth * 2.25 - p2 * minWidth * .75), maxDrawSpace - StickMan.height / 3 + (int)(p2 * (StickMan.height / 3 - 10)), Color.parseColor(color), 10);
			g.drawLine((int)(minWidth * 1.5), maxDrawSpace - 10, (int)(minWidth * 1.5 + p3 * minWidth * .75), maxDrawSpace - 10, Color.parseColor(color), 10);
			break;
		case 5:
			drawShadowedString("If you draw the bounce pad too high", y + 150, p, g);
			drawShadowedString("then you can't reach the top of it.", y + 225, p, g);
			break;
		case 6:
			drawShadowedString("Draw a banana to slide", y, p, g);
			drawShadowedString("through tight spaces.", y + 75, p, g);
			drawShadowedString("Using bananas will", y + 225, p, g);
			drawShadowedString("get you extra points.", y + 300, p, g);
			
			float p1b = clamp(percent, 0, 1 / 2f) * 2;
			float p2b = (clamp(percent, 1 / 2f, 1) - 1 / 2f) * 2;
			
			g.drawLine((int)(minWidth * 1.5) + StickMan.width / 3, maxDrawSpace, (int)(minWidth * 1.5 + StickMan.width / 3 + p1b * StickMan.width / 3), maxDrawSpace - (int)(p1b * StickMan.height / 2), Color.parseColor(color), 10);
			g.drawLine((int)(minWidth * 1.5) + StickMan.width * 2 / 3, maxDrawSpace - StickMan.height / 2, (int)(minWidth * 1.5 + StickMan.width * 2 / 3 + p2b * StickMan.width / 3), maxDrawSpace - StickMan.height / 2 + (int)(p2b * StickMan.height / 2), Color.parseColor(color), 10);
			break;
		case 7:
			drawShadowedString("That concludes the tutorial.", y, p, g);
			drawShadowedString("Thank you for choosing", y + 75, p, g);
			drawShadowedString("to play Sketch Runner.", y + 150, p, g);
			drawShadowedString("Have fun, and don't", y + 300, p, g);
			drawShadowedString("get eaten by the shark.", y + 375, p, g);
			break;
			
		default: return;
		}
	}
	
	private boolean onScreen(Rect bounds, Graphics g) {
		return !(bounds.left > screenWidth || bounds.right < 0 || bounds.top > screenHeight || bounds.bottom < 0);
	}
	
	private void drawShadowedString(String text, int y, Paint p, Graphics g) {
		Paint p2 = new Paint();
		p2.setColor(Color.BLACK);
		p2.setTextSize(p.getTextSize());
		
		float length = p2.measureText(text);
		int x = g.getWidth() / 2 - (int)(length / 2);
		
		g.drawString(text, x + 3, y + 3, p2);
		
		g.drawString(text, x, y, p);
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		if (softPause)
			ResourceManager.pauseAllSounds();
	}

	@Override
	public void dispose() {
		ResourceManager.stopAllSounds();
	}

	@Override
	public void backButton() {
		RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.ScoreWindow);
		if (window.getVisibility() == View.VISIBLE) {
			goToMainMenu();
			return;
		}
		
		window = (RelativeLayout)layout.findViewById(R.id.QuitWindow);
		if (window.getVisibility() == View.VISIBLE) {
			game.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.QuitWindow);
					window.setVisibility(View.GONE);
					
					if (wasInPause) {
						window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
						window.setVisibility(View.VISIBLE);
					} else {
						softPause = false;
						ResourceManager.resumeAllSounds();
					}
				}
				
			});
			return;
		}
		
		window = (RelativeLayout)layout.findViewById(R.id.HighScoresWindow);
		if (window.getVisibility() == View.VISIBLE)
		{
			game.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.HighScoresWindow);
					window.setVisibility(View.GONE);
					if (!stickMan.alive) {
						window = (RelativeLayout)layout.findViewById(R.id.ScoreWindow);
						window.setVisibility(View.VISIBLE);
					} else {
						window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
						window.setVisibility(View.VISIBLE);
					}
				}
			});
			return;
		}
		
		window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
		if (window.getVisibility() == View.VISIBLE) {
			softPause = false;
			wasInPause = false;
			ResourceManager.resumeAllSounds();
			
			game.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.pauseWindow);
					window.setVisibility(View.GONE);
				}
				
			});
			return;
		}
		
		if (!stickMan.alive) {
			showScore();
			return;
		}
		
		game.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				RelativeLayout window = (RelativeLayout)layout.findViewById(R.id.QuitWindow);
				window.setVisibility(View.VISIBLE);
				softPause = true;
				ResourceManager.pauseAllSounds();
			}
			
		});
	}
	
	public void goToMainMenu() {
		((Music)ResourceManager.get("Game", "BackgroundMusic")).stop();
		((Music)ResourceManager.get("Game", "DeathMusic")).stop();
		game.setScreen(new MainMenuScreen(game));
	}

}
