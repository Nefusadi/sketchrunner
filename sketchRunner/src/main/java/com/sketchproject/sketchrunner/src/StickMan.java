package com.sketchproject.sketchrunner.src;

import android.graphics.Rect;

import com.crystal.framework.Game;
import com.crystal.framework.Graphics;
import com.crystal.framework.resources.ResourceManager;
import com.crystal.framework.resources.Sound;
import com.crystal.framework.resources.Sprite;
import com.google.android.gms.games.Games;
import com.sketchproject.sketchrunner.R;
import com.sketchproject.sketchrunner.src.shapes.Collidable;

public class StickMan {
	GameScreen game;
	Game runnerGame;
	
	Animation[] anims = new Animation[6];
	int currentAnim = 0;
	
	int xPosition = 300; //pixels
	int yPosition = 700; //pixels
	static int height = 210; //pixels
	static int width = 140; //pixels
	
	Rect bounds = new Rect();
	
	int velocity = 0;
	
	final int gravity = 2500; //pixels / s^2 (155 pixels per meter)
	final static int maxRunSpeed = 500;
	public static int runSpeed = 0;
	
	public static int bottomSafeRange() { return height / 2; }
	public static int topSafeRange() { return height / 4; }
	
	int jumpStrength() { return (int)(-Math.sqrt(2 * gravity * 300)); }
	
	boolean isJumping = false;
	boolean isSliding = false;
	boolean onGround = true;
	boolean slideSoundPlaying = false;
	boolean footstepsPlaying = false;
	
	boolean alive = true;
	
	int collectionCount = 0;
	int bonusPoints = 0;
	
	int jumpCount = 0;
	long slideStart = 0;
	
	public StickMan(GameScreen game, Game runnerGame) {
		
		anims[0] = new Animation((Sprite)ResourceManager.get("Game", "Start"));
		anims[1] = new Animation((Sprite)ResourceManager.get("Game", "Run"));
		anims[2] = new Animation((Sprite)ResourceManager.get("Game", "Jump"));
		anims[3] = new Animation((Sprite)ResourceManager.get("Game", "Fall"));
		anims[4] = new Animation((Sprite)ResourceManager.get("Game", "Land"));
		anims[5] = new Animation((Sprite)ResourceManager.get("Game", "Slide"));
		
		this.game = game;
		this.runnerGame = runnerGame;
	}
	
	void switchAnim(int animIndex) {
		anims[currentAnim].restart();
		currentAnim = animIndex;
	}
	
	public void update(float deltaTime) {
		anims[currentAnim].update(deltaTime);
		
		if ((currentAnim == 0 || currentAnim == 4) && anims[currentAnim].done()) {
			switchAnim(1);
		} else if (currentAnim == 2 && (anims[currentAnim].done() || onGround))
			switchAnim(3);
		else if (isSpawning())
			return;
		
		if (isJumping) {
			if (anims[currentAnim].getRunningTime() > .1f) {
				isJumping = false;
				onGround = false;
				velocity = jumpStrength();
			}
			
			return;
		}
		
		if (isSliding && anims[currentAnim].done()) {
			stopSlide();
		} else if (isSliding && (int)((game.totalDistanceTraveled - slideStart) / 155) == 25 && runnerGame.getApiClient().isConnected())
				Games.Achievements.unlock(runnerGame.getApiClient(), runnerGame.getString(R.string.achievement_the_jungle));
		
		if (!isSliding && velocity > 150 && !onGround && currentAnim != 2) {
			boolean onDownSlope = false;
			if (currentAnim == 1 || currentAnim == 4) {
				Rect bounds = getBounds();
				for (Collidable c : game.env) {
					if (Rect.intersects(bounds, c.getBounds()))
						onDownSlope = true;
				}
			}
			
			if (!onDownSlope) {
				switchAnim(3);
				((Sound)ResourceManager.get("Sounds", "footsteps")).stopAll();
			}
		} else if (!isSliding && onGround && currentAnim == 3) {
			switchAnim(4);
			((Sound)ResourceManager.get("Sounds", "footsteps")).play(1);
			jumpCount = 0;
		} else if (isSliding && slideSoundPlaying && (!onGround && velocity > 150 || anims[currentAnim].getRunningTime() > anims[currentAnim].getTotalTime() - .3f)) {
			((Sound)ResourceManager.get("Sounds", "slide")).stopAll();
			
			if (onGround && anims[currentAnim].getRunningTime()  > anims[currentAnim].getTotalTime() - .3f) {
				((Sound)ResourceManager.get("Sounds", "footsteps")).play(1);
				footstepsPlaying = true;
			}
			else
				footstepsPlaying = false;
			
			slideSoundPlaying = false;
		} else if (isSliding && onGround && !slideSoundPlaying && anims[currentAnim].getRunningTime() < anims[currentAnim].getTotalTime() - .3f) {
			((Sound)ResourceManager.get("Sounds", "slide")).play(1);
			((Sound)ResourceManager.get("Sounds", "footsteps")).stopAll();
			slideSoundPlaying = true;
		} else if (!footstepsPlaying && isSliding && onGround && anims[currentAnim].getRunningTime() > anims[currentAnim].getTotalTime() - .3f) {
			((Sound)ResourceManager.get("Sounds", "footsteps")).play(1);
			footstepsPlaying = true;
		}
		
		// Apply gravity
		velocity += gravity * deltaTime;
		
		yPosition += velocity * deltaTime;
	}
	
	public Rect getBounds() {
		
		bounds.left = xPosition - getWidth();
		bounds.top = yPosition - getHeight();
		bounds.right = xPosition;
		bounds.bottom = yPosition;
		
		return bounds;
	}
	public void die() {
		alive = false;
		height = 210;
		width = 140;
		
		ResourceManager.stopAllSounds();
	}
	
	public void setOnGround(boolean onGround) {

		this.onGround = onGround;
	}
	
	public void setHeight(int height) {
		this.yPosition = height;
		velocity = 0;
	}
	
	public void incrementCollection() {
		collectionCount++;
		
		if (collectionCount == 200 && runnerGame.getApiClient().isConnected())
			Games.Achievements.unlock(runnerGame.getApiClient(), runnerGame.getString(R.string.achievement_artists_dream));
	}
	
	public int getYPosition() {
		return yPosition;
	}
	
	public int getXPosition() {
		return xPosition;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getVelocity() {
		return velocity;
	}
	
	public boolean getOnGround() {
		return onGround;
	}
	
	public void jump() {
		if (isSliding)
			stopSlide();
		
		jumpCount++;
		
		if (jumpCount == 10 && runnerGame.getApiClient().isConnected())
			Games.Achievements.unlock(runnerGame.getApiClient(), runnerGame.getString(R.string.achievement_airborn));
		
		isJumping = true;
		switchAnim(2);

		((Sound)ResourceManager.get("Sounds", "footsteps")).stopAll();
		((Sound)ResourceManager.get("Sounds", "bounce")).play(1);
	}
	
	public void slide() {
		if (isJumping)
			return;
		
		if (!isSliding)
			slideStart = game.totalDistanceTraveled;
		
		jumpCount = 0;
		if (isSliding) 
			anims[currentAnim].setFrame(10);
		else
			switchAnim(5);
		isSliding = true;
		height = 105;
		//width = 210;

		((Sound)ResourceManager.get("Sounds", "footsteps")).stopAll();
		((Sound)ResourceManager.get("Sounds", "slide")).stopAll();
		((Sound)ResourceManager.get("Sounds", "slide")).play(1);
		slideSoundPlaying = true;
	}
	
	private void stopSlide() {
		isSliding = false;
		switchAnim(1);
		height = 210;
		width = 140;
		
		((Sound)ResourceManager.get("Sounds", "slide")).stopAll();
		slideSoundPlaying = false;
	}
	
	public boolean getIsJumping() {
		return isJumping;
	}
	
	void respawn() {
		switchAnim(0);
		yPosition = 700;
	}
	
	public void paint(float deltaTime, Graphics g) {
		
		if (!alive) {
			if (currentAnim != 5)
				switchAnim(5);
			
			anims[currentAnim].update(deltaTime / 2);
		}
		
		anims[currentAnim].paint(g, xPosition, yPosition);
	}
	
	public boolean isSpawning() {
		return currentAnim == 0 && !anims[currentAnim].done();
	}
	public void AddBonusPoints() {
		bonusPoints += 5;
	}
}
