package com.sketchproject.sketchrunner.src;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.content.Context;
import android.os.Bundle;

import com.crystal.framework.Game;
import com.crystal.framework.Screen;
import com.google.android.gms.games.Games;
import com.sketchproject.sketchrunner.R;

public class RunnerGame extends Game {
	
	static HighScoreInfo highScores;
	
	@Override
	public Screen getInitScreen() {
		loadData();
		
		return new SplashScreen(this);
	}
	
	public void addScore(int score) {
		highScores.InsertScore(score);
		if (getApiClient().isConnected()) {
			Games.Leaderboards.submitScore(getApiClient(), getString(R.string.leaderboard_high_scores), score);
			
			if (score < 10)
				Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_shark_bait));
		}
		
		saveData();
	}
	
	void saveData() {
		try {
			FileOutputStream out = openFileOutput("highScores.spj", Context.MODE_PRIVATE);
			ObjectOutputStream ostream = new ObjectOutputStream(out);
			ostream.writeObject(highScores);
			ostream.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	void loadData() {
		try {
			FileInputStream in = openFileInput("highScores.spj");
			ObjectInputStream ostream = new ObjectInputStream(in);
			highScores = (HighScoreInfo)ostream.readObject();
			ostream.close();
			in.close();
		} catch (FileNotFoundException e) {
			highScores = new HighScoreInfo();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public int[] getScores() {
		return highScores.getScores();
	}
	
	public boolean getConnectToGoogle() {
		return highScores.getConnectToGoogle();
	}
	
    @Override
    public void onConnected(Bundle connectionHint) {
    	super.onConnected(connectionHint);
        // Connected to Google Play services!
        // The good stuff goes here.
    	highScores.setConnectToGoogle(true);
    	saveData();
    	
    	if (getCurrentScreen().getClass().getName() == MainMenuScreen.class.getName()) {
    		((MainMenuScreen)getCurrentScreen()).updateGooglePlayGUI();
    	} else if (getCurrentScreen().getClass().getName() == GameScreen.class.getName()) {
    		((GameScreen)getCurrentScreen()).updateGooglePlayGUI();
    	}
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);

    	if (!wasConnectedToGoogle) {
    		highScores.setConnectToGoogle(false);
    		saveData();
    	}

    	if (getCurrentScreen().getClass().getName() == MainMenuScreen.class.getName()) {
    		((MainMenuScreen)getCurrentScreen()).updateGooglePlayGUI();
    	}
    }
}
