package com.sketchproject.sketchrunner.src;

import java.io.Serializable;

public class HighScoreInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	boolean connectToGoogle = true;
	
	int[] scores = new int[10];
	
	public HighScoreInfo() {
		for (int i = 0; i < 10; i++)
			scores[i] = -1;
	}
	
	public void InsertScore(int score) {
		int index = 0;
		while (score < scores[index]) {
			index++;
			
			if (index == 10)
				return;
		}
		
		for (int i = 8; i >= index; i--)
			scores[i + 1] = scores[i];
		
		scores[index] = score;
	}
	
	public int[] getScores() {
		return scores;
	}
	
	public boolean getConnectToGoogle() {
		return connectToGoogle;
	}
	
	public void setConnectToGoogle(boolean connect) {
		connectToGoogle = connect;
	}
}
