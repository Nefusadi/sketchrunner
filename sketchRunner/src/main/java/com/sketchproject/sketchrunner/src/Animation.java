package com.sketchproject.sketchrunner.src;

import com.crystal.framework.Graphics;
import com.crystal.framework.resources.Sprite;

public class Animation {
	Sprite sprite;
	float playSpeed = 1;
	int currentFrame = 0;
	
	private float runningTime = 0;
	
	public Animation(Sprite sprite) {
		this.sprite = sprite;
	}
	
	public void update(float deltaTime) {
		
		runningTime += deltaTime * playSpeed;
		
		if (Math.abs(runningTime) > 1 / 30f) {
			int change = (int)(runningTime / (1 / 30f));
			currentFrame += change;
			
			runningTime -= change / 30f;
		}
	}
	
	public void paint(Graphics g, int x, int y) {
		// Use the bottom right corner, since that was kept consistent when making the sprites.
		g.drawSprite(sprite, currentFrame, x - sprite.getWidth() + 20, y - sprite.getHeight());
	}
	
	public void paint(Graphics g, int x, int y, int width, int height) {
		// used for the bounce pad, use the top left corner this time
		g.drawScaledSprite(sprite, currentFrame, x, y, width, height);
	}
	
	public void restart() {
		currentFrame = 0;
		runningTime = 0;
	}
	
	public void setSpeed(float newSpeed) {
		playSpeed = newSpeed;
	}
	
	public boolean done() {
		return currentFrame >= sprite.getTotalFrames();
	}
	
	public int getCurrentFrame() {
		return currentFrame;
	}
	
	public float getSpeed() {
		return playSpeed;
	}
	
	public float getRunningTime() {
		return (currentFrame / 30f) + runningTime;
	}
	
	public float getTotalTime() {
		return sprite.getTotalFrames() * (1 / 30f) / playSpeed;
	}
	
	public void setFrame(int frame) {
		currentFrame = frame % sprite.getTotalFrames();
	}
}
