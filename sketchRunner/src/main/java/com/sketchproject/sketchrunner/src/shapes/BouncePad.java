package com.sketchproject.sketchrunner.src.shapes;

import android.graphics.Rect;

import com.crystal.framework.Graphics;
import com.crystal.framework.resources.ResourceManager;
import com.crystal.framework.resources.Sprite;
import com.sketchproject.sketchrunner.src.Animation;
import com.sketchproject.sketchrunner.src.StickMan;

public class BouncePad implements Collidable {

	int x, y;
	int width, height;
	
	boolean playingAnim = false;
	boolean used = false;
	Animation anim;
	
	Rect bounds = new Rect();
	
	public BouncePad(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		anim = new Animation((Sprite)ResourceManager.get("Game", "BouncePad"));
	}
	
	@Override
	public Rect getBounds() {
		
		bounds.left = x;
		bounds.right = x + width;
		bounds.top = y;
		bounds.bottom = y + height;
		
		return bounds;
	}

	@Override
	public boolean collide(StickMan stickMan) {

		if (y < stickMan.getYPosition() - StickMan.bottomSafeRange())
			return false;

		if (stickMan.getYPosition() > y + height / 2)
			stickMan.setHeight(y + height / 2);
		
		if (!stickMan.getIsJumping() && stickMan.getVelocity() > 0) {
			stickMan.jump();
			
			if (!used)
				stickMan.AddBonusPoints();
			
			used = true;
		}
		
		playingAnim = true;
		
		return false;
	}

	@Override
	public void update(float deltaTime) {

		x = x - (int)(deltaTime * StickMan.runSpeed);
		
		if (playingAnim) {
			anim.update(deltaTime);
			if (anim.done()) {
				playingAnim = false;
				anim.restart();
			}
		}
	}

	@Override
	public void paint(float deltaTime, Graphics g) {
		anim.paint(g, x, y, width, height);
	}

	@Override
	public void paintPartial(float percent, Graphics g) {
		// TODO Auto-generated method stub
		
	}

}
