package com.sketchproject.sketchrunner.src;

import android.graphics.Color;

import com.crystal.framework.Game;
import com.crystal.framework.Graphics;
import com.crystal.framework.Graphics.ImageFormat;
import com.crystal.framework.Screen;
import com.crystal.framework.resources.*;

public class SplashScreen extends Screen {
	
	boolean finished = false;
	
	float alpha = 1;
	float fadeTimer = 1.5f;
	
	int imageScale = 1;
	
	public SplashScreen(Game game) {
		super(game);
		
		ResourceManager.loadImage("SketchProjectSplash.png", ImageFormat.RGB565, 1, "Splash", "Background");

	}
	
	@Override
	public void update(float deltaTime) {
		
		if (alpha > 0 || finished) // Only load once, and after the screen is done fading
			return;
		
		try {
			// Load main menu resources.
			ResourceManager.loadImage("MenuBackground.png", ImageFormat.RGB565, imageScale, "Menu", "Background");
			ResourceManager.loadMusic("Title.mp3", "Menu", "BackgroundMusic");
				
			((Music)ResourceManager.get("Menu", "BackgroundMusic")).setLooping(true);
			
			// Load game resources.
			ResourceManager.loadSprite("StickStart.png", ImageFormat.A8, imageScale, "Game", "Start", 10, 7);
			ResourceManager.loadSprite("StickRun.png", ImageFormat.A8, imageScale, "Game", "Run", 8, 5);
			ResourceManager.loadSprite("StickJump.png", ImageFormat.A8, imageScale, "Game", "Jump", 10, 3);
			ResourceManager.loadSprite("StickFall.png", ImageFormat.A8, imageScale, "Game", "Fall", 1, 1);
			ResourceManager.loadSprite("StickLand.png", ImageFormat.A8, imageScale, "Game", "Land", 3, 3);
			ResourceManager.loadSprite("StickSlide.png", ImageFormat.A8, imageScale, "Game", "Slide", 6, 10);
			ResourceManager.loadSprite("BouncePad.png", ImageFormat.RGB565, imageScale, "Game", "BouncePad", 4, 4);
			ResourceManager.loadSprite("Banana.png", ImageFormat.RGB565, imageScale, "Game", "Banana", 3, 5);
			ResourceManager.loadSprite("Collectable2.png", ImageFormat.A8, imageScale, "Game", "Collectable", 5, 6);
			
			ResourceManager.loadSprite("Shark.png", ImageFormat.A8, imageScale, "Game", "Shark", 4, 5);
			
			ResourceManager.loadImage("GameBackground.png", ImageFormat.RGB565, imageScale, "Game", "Background");
			ResourceManager.loadMusic("Stage.mp3", "Game", "BackgroundMusic");
			ResourceManager.loadMusic("Death.mp3", "Game", "DeathMusic");
			
			((Music)ResourceManager.get("Game", "BackgroundMusic")).setLooping(true);
			
			// Load sounds.
			ResourceManager.loadSound("boing.mp3", "Sounds", "bounce");
			ResourceManager.loadSound("chomp.wav", "Sounds", "chomp");
			ResourceManager.loadSound("button-click.wav", "Sounds", "click");
			ResourceManager.loadSound("collect.wav", "Sounds", "collect");
			ResourceManager.loadSound("sketchstart.wav", "Sounds", "drawingStart");
			ResourceManager.loadSound("sketch.wav", "Sounds", "drawing");
			ResourceManager.loadSound("slide.wav", "Sounds", "slide");
			ResourceManager.loadSound("footsteps.wav", "Sounds", "footsteps");
			
			((Sound)ResourceManager.get("Sounds", "drawingStart")).setLooping(true);
			((Sound)ResourceManager.get("Sounds", "drawing")).setLooping(true);
			((Sound)ResourceManager.get("Sounds", "footsteps")).setLooping(true);
		
			finished = true;
			
			//game.setScreen(new MainMenuScreen(game));
		} catch (OutOfMemoryError e) {
			ResourceManager.unloadGroup("Game");
			ResourceManager.unloadGroup("Menu");
			imageScale *= 2;
		}
	}

	@Override
	public void paint(float deltaTime) {
		Graphics g = game.getGraphics();
		g.clearScreen(-1);
		
		Image bg = (Image)ResourceManager.get("Splash", "Background");
		g.drawImage(bg, g.getWidth() / 2 - bg.getWidth() / 2, g.getHeight() / 2 - bg.getHeight() / 2);
		
		int drawColor = Color.argb((int)(alpha * 255), 255, 255, 255);
		
		if (alpha > 0 && fadeTimer > 0) {
			g.drawRect(0, 0, g.getWidth(), g.getHeight(), drawColor);
			
			alpha -= deltaTime * .75f;
		}
		else if (alpha <= 0 && fadeTimer > 0) {
			alpha = 0;
			fadeTimer -= deltaTime;
		}
		else if (fadeTimer <= 0 && finished && alpha < 1) {
			g.drawRect(0, 0, g.getWidth(), g.getHeight(), drawColor);
			
			alpha += deltaTime * .5f;
			if (alpha > 1)
				alpha = 1;
		}
		
		else if (fadeTimer <= 0 && alpha >= 1) {
			game.setScreen(new MainMenuScreen(game));
		}
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		ResourceManager.unloadGroup("Splash");
	}

	@Override
	public void backButton() {
		// disallow
	}

	
}
