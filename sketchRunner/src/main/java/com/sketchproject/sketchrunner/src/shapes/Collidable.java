package com.sketchproject.sketchrunner.src.shapes;

import com.crystal.framework.Graphics;
import com.sketchproject.sketchrunner.src.StickMan;

import android.graphics.Rect;

public interface Collidable {
	
	public Rect getBounds();
	
	public boolean collide(StickMan stickMan);
	
	public void update(float deltaTime);
	
	public void paint(float deltaTime, Graphics g);
	
	public void paintPartial(float percent, Graphics g);
}
